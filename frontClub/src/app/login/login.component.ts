import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { MemberService } from '../services/member.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private MemberService: MemberService,
    private cookie: CookieService,
    private router: Router
  ) {}
  ngOnInit(): void {
    console.log("ye BOIIi")
  }
  public handleLogin(login: NgForm): void {
    this.MemberService.login(login.value).subscribe(
      (data: any) => {
        console.log(data.refreshToken.token);
        console.log(data.token);
        console.log(data.member.roles);
        localStorage.setItem('token', data.token);
        localStorage.setItem('refresh', data.refreshToken.token);
        this.cookie.set('token', data.token);
        this.cookie.set('refresh', data.refreshToken.token);
        this.cookie.set('username', data.member.username);
        this.cookie.set('roles', JSON.stringify(data.member.roles));
        this.cookie.set('id', data.refreshToken.member.id);
        this.router.navigateByUrl('/members');
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          alert(error.error.message);
        }
      }
    );
  }
  public goBack(): void {
    this.router.navigateByUrl('/');
  }
}
