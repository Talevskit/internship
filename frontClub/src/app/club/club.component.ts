import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { MemberService } from '../services/member.service';
import { ClubService } from '../services/club.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Club } from '../models/club';
import { Member } from '../models/member';

@Component({
  selector: 'app-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.css'],
})
export class ClubComponent implements OnInit {
  public clubs!: Club[];
  public members!: Member[];
  public member: Member;
  public id: string;
  constructor(
    private MemberService: MemberService,
    private ClubService: ClubService,
    private cookie: CookieService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getClubs();
    this.getClubMemberships();
  }

  public clubDetails(id): void {
    try {
      this.router.navigateByUrl(`clubs/${id}`);
    } catch (error) {
      if (error.status === 401) {
        console.log(error.status);
        console.log('refreshing the token');
        this.MemberService.refresh().subscribe((data) => {
          console.log(data);
          this.cookie.deleteAll();
          this.cookie.set('token', data.token);
          this.cookie.set('refresh', data.refreshToken);
          console.log('refreshed the token, and calling getMember again');
        });
      }
    }
  }

  public getClubs(): void {
    this.ClubService.getClubs(this.cookie.get('token')).subscribe(
      (response: Club[]) => {
        console.log(response);
        this.clubs = response;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling getMembers again');
            this.ClubService.getClubs(this.cookie.get('token')).subscribe(
              (res) => {
                console.log(res);
                // location.reload();
                this.ngOnInit();
              }
            );
          });
        }
      }
    );
  }

  public getClubMemberships(): void {
    this.ClubService.getClubMemberships(this.cookie.get('token')).subscribe(
      (response: any[]) => {
        console.log(response);
        // this.clubs = response;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling getMembers again');
            this.ClubService.getClubs(this.cookie.get('token')).subscribe(
              (res) => {
                console.log(res);
                // location.reload();
                this.ngOnInit();
              }
            );
          });
        }
      }
    );
  }
  public addClub() {
    this.router.navigateByUrl('/addClub');
  }

  public addClubCheck(): boolean {
    if (this.cookie.get('roles').includes('ADMIN')) {
      return true;
    }
    return false;
  }
}
