import { Component, OnInit } from '@angular/core';
import { Member } from '../models/member';
import { MemberService } from '../services/member.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Byte } from '@angular/compiler/src/util';
@Component({
  selector: 'app-clubmember',
  templateUrl: './clubmember.component.html',
  styleUrls: ['./clubmember.component.css'],
})
export class ClubmemberComponent implements OnInit {
  public members!: Member[];
  public member: Member;
  public id: string;

  constructor(
    private MemberService: MemberService,
    private location: Location,
    private cookie: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getMembers();
  }

  public handleDelete(id): void {
    this.MemberService.deleteMember(id, this.cookie.get('token')).subscribe(
      (res) => {
        console.log(res);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling delete again');
            this.MemberService.deleteMember(
              id,
              this.cookie.get('token')
            ).subscribe((res) => {
              console.log(res);
            });
          });
        }
      }
    );
  }

  public getMember(): void {
    this.MemberService.getMember(this.id, this.cookie.get('token')).subscribe(
      (response: Member) => {
        console.log(response);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling getMember again');
            this.MemberService.getMember(
              this.id,
              this.cookie.get('token')
            ).subscribe((res) => {
              console.log(res);
            });
          });
        }
      }
    );
  }
  public editMember(id): void {
    try {
      this.router.navigateByUrl(`members/${id}`);
    } catch (error) {
      if (error.status === 401) {
        console.log(error.status);
        console.log('refreshing the token');
        this.MemberService.refresh().subscribe((data) => {
          console.log(data);
          this.cookie.deleteAll();
          this.cookie.set('token', data.token);
          this.cookie.set('refresh', data.refreshToken);
          console.log('refreshed the token, and calling getMember again');
        });
      }
    }
  }

  public getMembers(): void {
    this.MemberService.getMembers(this.cookie.get('token')).subscribe(
      (response: Member[]) => {
        console.log(response);
        this.members = response;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling getMembers again');
            this.MemberService.getMembers(this.cookie.get('token')).subscribe(
              (res) => {
                console.log(res);
                this.ngOnInit();
              }
            );
          });
        }
      }
    );
  }

  public getImage(id): boolean {
    this.MemberService.getImage(id).subscribe((res) => {
      console.log(res);
    });
    return false;
  }
}
