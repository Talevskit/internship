import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Club } from '../models/club';
import { ClubService } from '../services/club.service';

@Component({
  selector: 'app-add-club',
  templateUrl: './add-club.component.html',
  styleUrls: ['./add-club.component.css'],
})
export class AddClubComponent implements OnInit {
  constructor(
    private ClubService: ClubService,
    private cookie: CookieService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public handleAddClub(addClub: NgForm): void {
    let districtId = {
      id: addClub.value.districtId,
    };
    let club = {
      name: addClub.value.name,
      logo: addClub.value.logo,
      email: addClub.value.email,
      phone: addClub.value.phone,
      districtId: districtId,
      unionId: addClub.value.unionId,
      website: addClub.value.website,
      address: addClub.value.address,
      meetingTime: addClub.value.meetingTime,
      meetingPlace: addClub.value.meetingPlace,
    };
    console.log(club);
    this.ClubService.createClub(club, this.cookie.get('token')).subscribe(
      (res) => {
        console.log(res);
        this.router.navigateByUrl('/clubs');
      }
    );
  }
  public goBack(): void {
    this.router.navigateByUrl('/clubs');
  }
}
