import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Member } from '../models/member';
import { NgForm } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Byte } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root',
})
export class MemberService {
  constructor(private http: HttpClient, private cookie: CookieService) {}

  public getMembers(jwt: string): Observable<Member[]> {
    return this.http.get<Member[]>(`http://localhost:8080/api/members`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public getMember(id: string, jwt: string): Observable<Member> {
    return this.http.get<Member>(`http://localhost:8080/api/members/${id}`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }

  public saveMember(data: any): Observable<FormData> {
    return this.http.post<FormData>(`http://localhost:8080/api/members`, data);
  }
  public updateMember(
    member: Member,
    id: string,
    jwt: string
  ): Observable<Member> {
    return this.http.put<Member>(
      `http://localhost:8080/api/members/${id}`,
      member,
      {
        headers: {
          Authorization: 'Bearer ' + jwt,
        },
      }
    );
  }
  public deleteMember(id: string, jwt: string): Observable<Member> {
    return this.http.delete<Member>(`http://localhost:8080/api/members/${id}`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }

  public login(member: any): Observable<NgForm> {
    return this.http.post<any>(`http://localhost:8080/api/login`, member);
  }

  public refresh(): Observable<any> {
    let refresh = {
      refreshToken: this.cookie.get('refresh'),
    };
    return this.http.post<any>(
      `http://localhost:8080/api/refreshtoken`,
      refresh
    );
  }
  public getImage(id: string): Observable<any> {
    return this.http.get<any>(`http://localhost:8080/api/members/image/${id}`);
  }
  public getAvatar(id: string): Observable<any> {
    return this.http.get<any>(`http://localhost:8080/api/members/avatar/${id}`);
  }
}
