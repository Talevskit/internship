import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Club } from '../models/club';
import { Membership } from '../models/membership';

@Injectable({
  providedIn: 'root',
})
export class ClubService {
  constructor(private http: HttpClient, private cookie: CookieService) {}

  public getClubs(jwt: string): Observable<Club[]> {
    return this.http.get<Club[]>(`http://localhost:8080/api/clubs/`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public getClub(id: string, jwt: string): Observable<Club> {
    return this.http.get<Club>(`http://localhost:8080/api/clubs/${id}`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public getClubMemberships(jwt: string): Observable<any[]> {
    return this.http.get<any[]>(`http://localhost:8080/api/clubmemberships/`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public getClubMembershipsByClubId(id: any, jwt: string): Observable<any[]> {
    return this.http.get<any[]>(
      `http://localhost:8080/api/clubmemberships/byClub/${id}`,
      {
        headers: {
          Authorization: 'Bearer ' + jwt,
        },
      }
    );
  }
  public createClub(club: any, jwt: string): Observable<any> {
    return this.http.post<any>(`http://localhost:8080/api/clubs/`, club, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public deleteClub(id: any, jwt: string): Observable<Club> {
    return this.http.delete<Club>(`http://localhost:8080/api/clubs/${id}`, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public deleteClubMembership(id: any, jwt: string): Observable<Membership> {
    return this.http.delete<Membership>(
      `http://localhost:8080/api/clubmemberships/${id}`,
      {
        headers: {
          Authorization: 'Bearer ' + jwt,
        },
      }
    );
  }
  public updateClub(id: string, club: any, jwt: string): Observable<Club> {
    return this.http.put<Club>(`http://localhost:8080/api/clubs/${id}`, club, {
      headers: {
        Authorization: 'Bearer ' + jwt,
      },
    });
  }
  public joinClub(membership: any, jwt: string): Observable<any> {
    return this.http.post<any>(
      `http://localhost:8080/api/clubmemberships`,
      membership,
      {
        headers: {
          Authorization: 'Bearer ' + jwt,
        },
      }
    );
  }
}
