import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Member } from '../models/member';
import { MemberService } from '../services/member.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  public file: any;
  constructor(
    private MemberService: MemberService,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {}
  selectedFile: File = null;

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
  }
  public handleRegister(register: NgForm): void {
    // let formData = new FormData();
    // formData.set('member', JSON.stringify(register.value));
    // formData.set('file', this.selectedFile, this.selectedFile.name);
    this.MemberService.saveMember(register.value).subscribe(
      (res) => {
        console.log(res);
        this.router.navigateByUrl('/members');
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          alert(error.error.message);
        }
      }
    );
  }
  public goBack(): void {
    this.router.navigateByUrl('/');
  }
}
