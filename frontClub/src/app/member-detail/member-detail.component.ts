import { Component, OnInit, Input } from '@angular/core';
import { Member } from '../models/member';
import { MemberService } from '../services/member.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css'],
})
export class MemberDetailComponent implements OnInit {
  public member!: Member;
  public id: string;
  constructor(
    private MemberService: MemberService,
    private route: ActivatedRoute,
    private router: Router,
    private cookie: CookieService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });
    this.getMember(this.id);
  }
  public getMember(id): void {
    this.MemberService.getMember(id, this.cookie.get('token')).subscribe(
      (res: any) => {
        this.member = res.member;
        console.log(this.member);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling update again');
            this.router.navigateByUrl(`/members/${id}`);
            this.MemberService.getMember(
              id,
              this.cookie.get('token')
            ).subscribe((res) => {
              console.log(res);
              this.ngOnInit();
            });
          });
        }
      }
    );
  }

  public handleUpdate(update: NgForm): void {
    this.MemberService.updateMember(
      update.value,
      this.id,
      this.cookie.get('token')
    ).subscribe(
      (res) => {
        console.log(res);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          console.log('All fields are required');
        } else if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling update again');
            this.MemberService.updateMember(
              update.value,
              this.id,
              this.cookie.get('token')
            ).subscribe((res) => {
              console.log(res);
              this.router.navigateByUrl('/members');
            });
          });
        }
      }
    );
  }

  public goBack(): void {
    this.router.navigateByUrl('/members');
  }
}
