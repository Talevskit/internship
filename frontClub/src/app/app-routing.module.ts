import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddClubComponent } from './add-club/add-club.component';
import { ClubDetailsComponent } from './club-details/club-details.component';
import { ClubComponent } from './club/club.component';
import { ClubmemberComponent } from './clubmember/clubmember.component';
import { ClubmembershipsComponent } from './clubmemberships/clubmemberships.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MemberDetailComponent } from './member-detail/member-detail.component';
import { MembershipfeesComponent } from './membershipfees/membershipfees.component';
import { PrebidComponent } from './prebid/prebid.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path: 'members', component: ClubmemberComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'members/:id', component: MemberDetailComponent },
  { path: '', component: HomeComponent },
  { path: 'clubs', component: ClubComponent },
  { path: 'clubs/:id', component: ClubDetailsComponent },
  { path: 'addClub', component: AddClubComponent },
  { path: 'prebid', component: PrebidComponent },

  { path: 'clubmemberships', component: ClubmembershipsComponent },
  { path: 'clubmembershipfees', component: MembershipfeesComponent },
  { path: 'membershipfees', component: MembershipfeesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
