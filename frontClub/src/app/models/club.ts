import { Member } from './member';
export class Club {
  club: {
    address: string;
    districtId: {
      id: number;
    };
    email: string;
    logo: string;
    meetingPlace: string;
    meetingTime: number;
    name: string;
    phone: number;
    unionId: number;
    website: string;
  };
  id: {
    id: string;
  };
  members: [
    {
      member: Member;
    }
  ];
}
