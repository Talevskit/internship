export class Membership {
  membershipId: number;
  club: {
    name: string;
    logo: string;
    email: string;
    phone: number;
    districtId: {
      id: number;
    };
    unionId: number;
    website: string;
    address: string;
    meetingTime: number;
    meetingPlace: string;
  };
  memberResponse: {
    name: number;
    surname: string;
    username: string;
    email: string;
    gender: string;
    age: number;
    occupation: string;
    company: string;
    phone: number;
    roles: [];
    // clubMemberships: null,
    // membershipFee: null
  };
}
