export class Member {
    id: number;
    name: string;
    surname:string;
    age:number;
    email:string;
    phone:number;
    image:string;
    gender:string;
    occupation:string;
    company:string;
    username:string;
    password:number;

}