import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { MemberService } from '../services/member.service';
import { ClubService } from '../services/club.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Club } from '../models/club';
import { Member } from '../models/member';
import { error } from '@angular/compiler/src/util';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Membership } from '../models/membership';

@Component({
  selector: 'app-club',
  templateUrl: './club-details.component.html',
  styleUrls: ['./club-details.component.css'],
})
export class ClubDetailsComponent implements OnInit {
  closeResult = '';
  public club!: Club;
  public members!: Member[];
  public member: Member;
  public memberships!: Membership[];
  public id: string;
  constructor(
    private route: ActivatedRoute,
    private MemberService: MemberService,
    private ClubService: ClubService,
    private cookie: CookieService,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
    });
    this.getClub(this.id);
    this.getClubMembershipsByClubId(this.id);
  }

  public handleDelete(id): void {
    this.ClubService.deleteClub(id, this.cookie.get('token')).subscribe(
      (res) => (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling delete again');
            this.ClubService.deleteClub(id, this.cookie.get('token'));
          });
        }
      }
    );
    this.router.navigateByUrl('/clubs');
  }

  public getClub(id): void {
    this.ClubService.getClub(id, this.cookie.get('token')).subscribe(
      (response: Club) => {
        this.club = response;
        console.log(this.club);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling getMembers again');
            this.ClubService.getClub(id, this.cookie.get('token')).subscribe(
              (res) => {
                console.log(res);
                this.ngOnInit();
              }
            );
          });
        }
      }
    );
  }
  public getClubMembershipsByClubId(id): void {
    this.ClubService.getClubMembershipsByClubId(
      id,
      this.cookie.get('token')
    ).subscribe(
      (res: Membership[]) => {
        this.memberships = res;
        console.log(res);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling getMembers again');
            this.ClubService.getClub(id, this.cookie.get('token')).subscribe(
              (res) => {
                console.log(res);
                this.ngOnInit();
              }
            );
          });
        }
      }
    );
  }

  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  public joinClubHandler() {
    let membership = {
      clubId: {
        id: this.id,
      },
      memberId: {
        id: this.cookie.get('id'),
      },
      to: 2022,
    };
    this.ClubService.joinClub(membership, this.cookie.get('token')).subscribe(
      (res: any) => {
        console.log(res);
        this.ngOnInit();
        alert('Club joined succesfully!');
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log(error.status);
          console.log('refreshing the token');
          this.MemberService.refresh().subscribe((data) => {
            console.log(data);
            this.cookie.deleteAll();
            this.cookie.set('token', data.token);
            this.cookie.set('refresh', data.refreshToken);
            console.log('refreshed the token, and calling joinClub again');
            this.ClubService.joinClub(
              membership,
              this.cookie.get('token')
            ).subscribe((res) => {
              console.log(res);
              this.ngOnInit();
            });
          });
        }
      }
    );
  }

  public adminCheck(): boolean {
    let idk: any = [];
    this.club.members.map((x) => {
      idk.push(x.member.username);
    });
    if (
      this.cookie.get('roles').includes('ADMIN') ||
      idk.includes(this.cookie.get('username'))
    ) {
      return false;
    }
    return true;
  }
  public actionsCheck(): boolean {
    if (this.cookie.get('roles').includes('ADMIN')) {
      return true;
    }
    return false;
  }
  public handleDeleteMembership(id): void {
    this.ClubService.deleteClubMembership(
      id,
      this.cookie.get('token')
    ).subscribe((res) => (error: HttpErrorResponse) => {
      if (error.status === 401) {
        console.log(error.status);
        console.log('refreshing the token');
        this.MemberService.refresh().subscribe((data) => {
          console.log(data);
          this.cookie.deleteAll();
          this.cookie.set('token', data.token);
          this.cookie.set('refresh', data.refreshToken);
          console.log('refreshed the token, and calling delete again');
          this.ClubService.deleteClubMembership(id, this.cookie.get('token'));
        });
      }
    });
    this.ngOnInit();
  }
}
