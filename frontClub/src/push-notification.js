import firebase from "firebase";

export const askForPermissioToReceiveNotifications = async () => {
  try {
    const messaging = firebase.messaging();
    console.log("000ooppp", messaging);
    messaging
      .requestPermission()
      .then(function () {
        console.log("I am in here");

        return messaging.getToken().then(function (currentToken) {
          console.log(currentToken);
        });
      })
      .catch(function (err) {
        console.log("Unable to get permission to notify.", err);
      });
  } catch (error) {
    console.error(error);
  }
};
