// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBpHzf9mxSr2Zd42enRyWZ-JBJZ5cDD8Y8',
    authDomain: 'inellipse-43d5c.firebaseapp.com',
    projectId: 'inellipse-43d5c',
    storageBucket: 'inellipse-43d5c.appspot.com',
    messagingSenderId: '305917122766',
    appId: '1:305917122766:web:25579b18148842c64c989f',
    measurementId: 'G-3EX1Y5ZK8Q',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
