package com.inellipse.clubmanagement;

import java.io.IOException;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.inellipse.clubmanagement.application.serviceimpl.AvatarServiceImpl;
import com.inellipse.clubmanagement.domain.model.*;
import com.inellipse.clubmanagement.domain.repository.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.authentication.Username;

@Component
public class DataGenerator {

    private final MemberRepository memberRepository;
    private final ClubRepository clubRepository;
    private final ClubMembershipRepository clubMembershipRepository;
    private final LanguageRepository languageRepository;
    private final MembershipFeeRepository membershipFeeRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final DistrictRepository districtRepository;
    private final UnionRepository unionRepository;

    private final AvatarServiceImpl avatar;

    public DataGenerator(MemberRepository memberRepository, ClubRepository clubRepository, ClubMembershipRepository clubMembershipRepository, LanguageRepository languageRepository, MembershipFeeRepository membershipFeeRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, DistrictRepository districtRepository, UnionRepository unionRepository, AvatarServiceImpl avatar) {
        this.memberRepository = memberRepository;
        this.clubRepository = clubRepository;
        this.clubMembershipRepository = clubMembershipRepository;
        this.languageRepository = languageRepository;
        this.membershipFeeRepository = membershipFeeRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.districtRepository = districtRepository;
        this.unionRepository = unionRepository;
        this.avatar = avatar;
    }

    @PostConstruct
    @Transactional
    public void generateData() throws IOException {

        Set<Member> members;
        Set<Club> clubs;
        Set<ClubMembership> memberships;
        Set<MembershipFee> membershipFees;
        Set<Role> roles;
        Set<District> districts;
        Set<Union> unions;

        if(roleRepository.findAll().size() == 0){
            roles = new HashSet<>();
            roles.add(createRole("MEMBER"));
            roles.add(createRole("OWNER"));
            roles.add(createRole("ADMIN"));
            roleRepository.saveAll(roles);
        }

        if(districtRepository.findAll().size() == 0){
            districts = new HashSet<>();
            districts.add(createDistrict(new DistrictId("1"),"Jugoistochen"));
            districts.add(createDistrict(new DistrictId("2"),"Skopski"));
            districts.add(createDistrict(new DistrictId("3"),"Pelagoniski"));
            districts.add(createDistrict(new DistrictId("4"),"Vardarski"));
            districts.add(createDistrict(new DistrictId("5"),"Jugozapaden"));
            districtRepository.saveAll(districts);
        }

        if(unionRepository.findAll().size() == 0){
            unions = new HashSet<>();
            unions.add(createUnion(new UnionId("1"),"Students"));
            unions.add(createUnion(new UnionId("2"),"Employees"));
            unions.add(createUnion(new UnionId("3"),"Interns"));
            unionRepository.saveAll(unions);
        }


        if (memberRepository.findAll().size() == 0) {
            members = new HashSet<>();

           members.add(createMember(new MemberId("1"),"Ljubica","Boneva",23,"ljubicaboneva@gmail.com", (long) 075222111,
                    "female","Intern","Inellipse", Username.valueOf("ljubica"),passwordEncoder.encode("1234"), true));

            members.add(createMember(new MemberId("2"),"Sara","Mojsova",22,"sara@gmail.com", (long) 075555222,
                    "female","Intern","Inellipse", Username.valueOf("sara"),passwordEncoder.encode("1234"),true));

            members.add(createMember(new MemberId("3"),"Stefan","Minchevski",27,"stefan@gmail.com", (long) 075555222,
                   "male","Intern","Inellipse", Username.valueOf("stefan"),passwordEncoder.encode("1234"),true));

            members.add(createMember(new MemberId("4"),"Bojan","Siljanoski",22,"bojan@gmail.com", (long) 075555222,
                   "male","Intern","Inellipse", Username.valueOf("bojan"),passwordEncoder.encode("1234"),true));

            members.add(createMember(new MemberId("5"),"Petko","Petkov",45,"petko@gmail.com", (long) 075555222,
                    "male","Gradeznik","Zikol", Username.valueOf("petko"),passwordEncoder.encode("1234"),true));

            members.add(createMember(new MemberId("6"),"Tino","Talevski",26,"tino@gmail.com", (long) 075555222,
                    "male","Intern","Inellipse", Username.valueOf("tino"),passwordEncoder.encode("1234"),true));
            memberRepository.saveAll(members);
            
            addRoleToMember("1", (long) 1);
            addRoleToMember("3", (long) 3);
            addRoleToMember("4", (long) 1);
            addRoleToMember("5", (long) 1);
            addRoleToMember("5", (long) 2);
            addRoleToMember("5", (long) 3);
            addRoleToMember("1", (long) 3);
            addRoleToMember("6", (long) 3);
        }

        if (clubRepository.findAll().size() == 0){
            clubs = new HashSet<>();
            clubs.add(createClub(new ClubId("1"),"Club1","url","club@gmail.com",(long)071255555,
                    new DistrictId("1"),new UnionId("1"),"web","adresa broj 2",new Date(2021,06,06,11,30),"online",1000));
            clubs.add(createClub(new ClubId("2"),"Club2","url","club2@gmail.com",(long)071255555,
                    new DistrictId("3"),new UnionId("2"),"web","adresa broj 3",new Date(2021,07,06,10,30),"online",2000));
            clubRepository.saveAll(clubs);
        }

        if(clubMembershipRepository.findAll().size() == 0){
            memberships = new HashSet<>();
            memberships.add(createMembership(new ClubId("1"),new MemberId("1"),Date.from(java.time.ZonedDateTime.now(ZoneOffset.UTC).toInstant()),2020));
            memberships.add(createMembership(new ClubId("1"),new MemberId("2"),Date.from(java.time.ZonedDateTime.now(ZoneOffset.UTC).toInstant()),2020));
            memberships.add(createMembership(new ClubId("2"),new MemberId("3"),Date.from(java.time.ZonedDateTime.now(ZoneOffset.UTC).toInstant()),2020));
            memberships.add(createMembership(new ClubId("2"),new MemberId("5"),Date.from(java.time.ZonedDateTime.now(ZoneOffset.UTC).toInstant()),2020));
            clubMembershipRepository.saveAll(memberships);
        }

        if(membershipFeeRepository.findAll().size() == 0){
            membershipFees = new HashSet<>();
            membershipFees.add(createMembershipFee((long)1,1000,11,new MemberId("1"),false,200,new ClubId("1")));
            membershipFees.add(createMembershipFee((long)2,2000,11,new MemberId("3"),true,2000,new ClubId("2")));
            membershipFees.add(createMembershipFee((long)3,1000,11,new MemberId("2"),true,1000,new ClubId("1")));
            membershipFeeRepository.saveAll(membershipFees);
        }
    }

    private Member createMember(MemberId memberId, String name, String surname, int age, String email,
                                Long phone, String gender, String occupation, String company,
                                Username username, String password, boolean enabled){

        return new Member(memberId, name,surname,age,email,phone,gender,occupation,company,username,password,enabled);
    }

    private Club createClub(ClubId clubId, String name, String logo, String email, long phone, DistrictId districtId,
                            UnionId unionId, String website, String address, Date meetingTime,String meetingPlace,long price){
        return new Club(clubId,name,logo,email,phone,districtId,unionId,website,address,meetingTime,meetingPlace,price);
    }

    private ClubMembership createMembership(ClubId clubId, MemberId memberId,
                                            Date since, int to){
        return new ClubMembership(clubId,memberId,to);
    }

    private MembershipFee createMembershipFee(Long id, int amount, int paidOn, MemberId memberId,boolean paid, int amountPaid,ClubId clubId){
        return new MembershipFee(id,amount,paidOn,memberId,paid,amountPaid,clubId);
    }
    

    private Role createRole(String name){
        return new Role(name);
    }

    private District createDistrict(DistrictId id,String name){
        return new District(id,name);
    }

    private Union createUnion(UnionId id, String name){
        return new Union(id,name);
    }

    private void addRoleToMember(String memberId, Long roleId) {
        Member member = memberRepository.findById(new MemberId(memberId)).get();
        Role role = roleRepository.findById(roleId);
        member.addRole(role);
        memberRepository.save(member);
    }
}