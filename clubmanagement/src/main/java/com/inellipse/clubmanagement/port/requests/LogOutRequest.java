package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.MemberId;
import lombok.Getter;

@Getter
public class LogOutRequest {

    private MemberId memberId;

}
