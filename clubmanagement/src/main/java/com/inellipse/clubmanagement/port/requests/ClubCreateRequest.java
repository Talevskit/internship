package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.DistrictId;
import com.inellipse.clubmanagement.domain.model.UnionId;
import com.sun.istack.NotNull;
import lombok.Getter;

import java.util.Date;


@Getter
public class ClubCreateRequest {

    @NotNull
    ClubId clubId;

    @NotNull
    String name;

    String logo;

    @NotNull
    String email;

    @NotNull
    long phone;

    DistrictId districtId;

    UnionId unionId;

    String website;

    @NotNull
    String address;

    Date meetingTime;

    String meetingPlace;

    @NotNull
    long price;

}
