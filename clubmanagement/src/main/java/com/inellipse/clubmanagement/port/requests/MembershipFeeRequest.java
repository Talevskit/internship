package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.sun.istack.NotNull;
import lombok.Getter;


@Getter
public class MembershipFeeRequest {

    @NotNull
    private long id;

    @NotNull
    private int paidOn;

    @NotNull
    private MemberId memberId;

    @NotNull
    private boolean paid;

    @NotNull
    private long amountPaid;

    @NotNull
    private ClubId clubId;

}