package com.inellipse.clubmanagement.port.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetNewPasswordRequest {

    private String password;

    private String token;

}
