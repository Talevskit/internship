package com.inellipse.clubmanagement.port.api;


import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inellipse.clubmanagement.application.service.ClubMembershipService;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.ClubMembership;
import com.inellipse.clubmanagement.domain.model.ClubMembershipsDto;
import com.inellipse.clubmanagement.port.requests.ClubMembershipCreateRequest;
@CrossOrigin
@RestController
@RequestMapping("/api/clubmemberships")
public class ClubMembershipController {

    private final ClubMembershipService clubMembershipService;

    public ClubMembershipController(ClubMembershipService clubMembershipService) {
        this.clubMembershipService = clubMembershipService;
    }

    @GetMapping
    public List<ClubMembershipsDto> getClubMemberships() {
        return clubMembershipService.getClubMemberships();
    }

    @GetMapping("/{id}")
    public ClubMembershipsDto getClubMembership(@PathVariable Long id) {
        return clubMembershipService.getClubMembership(id);
    }
    
    @GetMapping("/byClub/{clubId}")
    public List<ClubMembershipsDto> getClubMembershipByClubId(@PathVariable ClubId clubId) {
        return clubMembershipService.getClubMembershipByClubId(clubId);
    }

    @PostMapping
    public ClubMembership createNewClubMembership (@RequestBody ClubMembershipCreateRequest request) {
        return clubMembershipService.createClubMembership(request);
    }

    @DeleteMapping("/{id}")
    public void deleteClubMembership (@PathVariable Long id){
        clubMembershipService.deleteByClubMembershipId(id);
    }
}
