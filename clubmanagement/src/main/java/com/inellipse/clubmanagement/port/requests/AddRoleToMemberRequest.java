package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.sun.istack.NotNull;
import lombok.Getter;

@Getter
public class AddRoleToMemberRequest {

    @NotNull
    MemberId memberId;

    @NotNull
    Role role;


}
