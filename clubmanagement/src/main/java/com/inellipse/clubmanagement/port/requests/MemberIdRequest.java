package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.MemberId;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberIdRequest {

    @NotNull
    MemberId memberId;

}
