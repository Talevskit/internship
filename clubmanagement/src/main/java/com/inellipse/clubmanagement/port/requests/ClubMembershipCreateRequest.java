package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class ClubMembershipCreateRequest {

    @NotNull
    private ClubId clubId;

    @NotNull
    private MemberId memberId;

    @NotNull
    private int to;
}
