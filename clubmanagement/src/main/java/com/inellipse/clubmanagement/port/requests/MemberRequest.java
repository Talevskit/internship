package com.inellipse.clubmanagement.port.requests;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.ClubMembership;
import com.inellipse.clubmanagement.domain.model.Language;
import com.inellipse.clubmanagement.domain.model.Link;
import com.inellipse.clubmanagement.domain.model.MembershipFee;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class MemberRequest {

    private String name;

    private String surname;

    private Integer age;

    private String email;

    private Long phone;

    private List<Link> links;

    private List<Language> languages;

    private String gender;

    private String occupation;

    private String company;

    private Set<ClubMembership> clubMemberships;
    private Set<MembershipFee> MembershipFee;
    private Set<Role> roles;


    private Username username;

    private String password;

    private boolean enabled;
    

   /* public String setImage(){
        if(gender.equals("male")){
            this.image = "src\\main\\resources\\images\\avatar-male.png";
        }
        else{
            this.image = "src\\main\\resources\\images\\avatar-female.png";
        }
        return this.image;
    }*/

}
