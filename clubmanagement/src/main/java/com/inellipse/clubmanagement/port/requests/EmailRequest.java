package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.Member;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailRequest {


	private String to;
	private String subject;
	private String body;

}
