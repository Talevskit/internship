package com.inellipse.clubmanagement.port.response;

import java.util.Set;
import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.ClubMembership;
import com.inellipse.clubmanagement.domain.model.MembershipFee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberResponse {
	private String name;
	private String surname;
    Username username;
	private String email;
	private String gender;
	private Integer age;
	private String occupation;
	private String company;
	private Long phone;
	private Set<Role> roles;
	Set<ClubMembership> clubMemberships;
    Set<MembershipFee> MembershipFee;

}
