package com.inellipse.clubmanagement.port.api;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.inellipse.clubmanagement.application.serviceimpl.FirebaseMessagingService;
import com.inellipse.clubmanagement.port.requests.Note;
import org.apache.tomcat.util.json.ParseException;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
public class FireBaseMessagingController {

    private final FirebaseMessagingService firebaseService;

    public FireBaseMessagingController(FirebaseMessagingService firebaseService) {
        this.firebaseService = firebaseService;
    }

    @RequestMapping("/send-notification")
    @ResponseBody
    public String sendNotification(@RequestBody Note note,
                                   @RequestParam String id) throws FirebaseMessagingException, FirebaseAuthException, IOException {
        return firebaseService.sendNotification(note,id);
    }

    @RequestMapping("/send-notification-topic")
    @ResponseBody
    public String sendNotificationTopic(@RequestBody Note note,
                                   @RequestParam String topic) throws FirebaseMessagingException, FirebaseAuthException, IOException {
        return firebaseService.sendNotificationWithTopic(note,topic);
    }

    @GetMapping("/firebase/{firebase}")
    @ResponseBody
    public String getToken(@PathVariable String firebase, @RequestParam String id) throws FirebaseMessagingException, FirebaseAuthException, IOException {
        return firebaseService.setToken(firebase,id);
    }

    @GetMapping("/info")
    @ResponseBody
    public String getInfo() throws IOException, JSONException, ParseException {
       return firebaseService.retrieveData();
    }

}
