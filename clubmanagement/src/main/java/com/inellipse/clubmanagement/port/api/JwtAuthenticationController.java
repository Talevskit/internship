package com.inellipse.clubmanagement.port.api;

import com.inellipse.clubmanagement.application.service.jwtservice.AuthenticationService;
import com.inellipse.clubmanagement.application.serviceimpl.RefreshTokenServiceImpl;
import com.inellipse.clubmanagement.configuration.JwtTokenUtil;
import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.RefreshToken;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.exception.ApiException;
import com.inellipse.clubmanagement.exception.TokenRefreshException;
import com.inellipse.clubmanagement.port.requests.AuthenticationRequest;
import com.inellipse.clubmanagement.port.requests.LogOutRequest;
import com.inellipse.clubmanagement.port.requests.TokenRefreshRequest;
import com.inellipse.clubmanagement.port.response.AuthenticationResponse;
import com.inellipse.clubmanagement.port.response.TokenRefreshResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class JwtAuthenticationController {

    private final AuthenticationService service;

    private final RefreshTokenServiceImpl refreshTokenService;

    private final MemberRepository memberRepository;

    @Autowired
    private JwtTokenUtil jwtUtil;

    public JwtAuthenticationController(AuthenticationService service, RefreshTokenServiceImpl refreshTokenService, MemberRepository memberRepository) {
        this.service = service;
        this.refreshTokenService = refreshTokenService;
        this.memberRepository = memberRepository;
    }
    @PostMapping("/login")
    public AuthenticationResponse createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
    	Member member = memberRepository.findByUsername(new Username(authenticationRequest.getUsername()));
    	if(member.isEnabled() == false)
    		throw new ApiException("Account not verified!");
    	this.service.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
    	return this.service.generateToken(authenticationRequest.getUsername());
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getMember)
                .map(user -> {
                    Optional<Member> member = memberRepository.findById(user);
                    String token = jwtUtil.generateTokenFromUsername(member.get().getUsername().getValue());
                    return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }

    @DeleteMapping("/logout")
    public ResponseEntity<?> logoutUser(@Valid @RequestBody LogOutRequest logOutRequest) {
        refreshTokenService.deleteByMemberId(logOutRequest.getMemberId());
        return ResponseEntity.ok("Log out successful!");
    }




}