package com.inellipse.clubmanagement.port.requests;

import java.util.Map;

import lombok.Data;

@Data
public class Note {
    private String subject;
    private String content;

}