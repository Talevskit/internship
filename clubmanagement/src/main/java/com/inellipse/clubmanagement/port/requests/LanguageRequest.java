package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.MemberId;
import com.sun.istack.NotNull;
import lombok.Getter;

@Getter
public class LanguageRequest {

    @NotNull
    Long id;

    @NotNull
    String languageName;

    @NotNull
    MemberId memberId;

}
