package com.inellipse.clubmanagement.port.api;


import java.util.List;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.inellipse.clubmanagement.application.serviceimpl.AvatarServiceImpl;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.domain.repository.RoleRepository;
import com.inellipse.clubmanagement.port.requests.AddRoleToMemberRequest;
import com.inellipse.clubmanagement.port.requests.PayMembershipFeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.inellipse.clubmanagement.application.serviceimpl.MemberServiceImpl;
import com.inellipse.clubmanagement.domain.model.MemberDto;
import com.inellipse.clubmanagement.port.requests.MemberRequest;
import com.inellipse.clubmanagement.port.response.MemberResponse;

import javax.mail.MessagingException;
import javax.management.relation.RoleNotFoundException;


@CrossOrigin
@RestController
@RequestMapping(path = "api/members")
public class MemberController {

	@Autowired
    private MemberServiceImpl memberService;

    @Autowired
	private AvatarServiceImpl avatarService;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping
    public List<MemberDto> getMembers() {
        return this.memberService.getMembers();
    }
    
    @PostMapping
    public MemberResponse registerNewMember(@RequestBody MemberRequest member) {
        return memberService.createMember(member);
    }

    @DeleteMapping("/{id}")
    public void deleteMember(@PathVariable String id) {
        memberService.deleteById(id);
    }

    @GetMapping("/{id}")
    public MemberDto getMemberById(@PathVariable String id) {
        return memberService.getById(id);
    }

    @PutMapping("/{id}")
    public Member updateMember(@PathVariable String id,@RequestBody MemberRequest member) {
        return memberService.updateMember(id, member);
    }

    @GetMapping("/avatar/{id}")
    public byte[] getImage(@PathVariable String id) throws IOException {
        return avatarService.createAvatar();
    }
    
    @GetMapping(value = "/image/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> image(@PathVariable String id) throws IOException {
        final ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(
                "C:\\Users\\Tino\\repo\\ClubManagementBackEnd\\clubmanagement\\src\\main\\resources\\images\\"+id+".jpg"
        )));
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentLength(inputStream.contentLength())
                .body(inputStream);

    }

    @PostMapping("/addRole")
    public String setRoleToMember(@RequestBody AddRoleToMemberRequest request){
        roleRepository.findAll().stream().forEach(role -> {
            if(!role.getId().toString().equals(request.getRole().getId().toString())){
                try {
                    throw new RoleNotFoundException(request.getRole().getName());
                } catch (RoleNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        Member member = memberRepository.findById(request.getMemberId()).get();
        member.addRole(request.getRole());
        member =  memberRepository.saveAndFlush(member);
        memberRepository.save(member);
        return "Role added to member with username: " + member.getUsername();
    }

    @PostMapping("/pay")
    public void pay(@RequestBody PayMembershipFeeRequest request){
        memberService.payMembershipFee(request.getSum(),request.getMemberId(),request.getClubId());
    }

    @GetMapping("/lottery")
    @ResponseBody
    public Member get() throws MessagingException, IOException {
        return memberService.lottery();
    }

}
