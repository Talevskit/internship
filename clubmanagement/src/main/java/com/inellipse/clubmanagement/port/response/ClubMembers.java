package com.inellipse.clubmanagement.port.response;

import java.util.List;

import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.MemberDto;

import lombok.Data;

@Data
public class ClubMembers {
	private ClubId id;
	private Club club;
	private List<MemberDto> members;

}
