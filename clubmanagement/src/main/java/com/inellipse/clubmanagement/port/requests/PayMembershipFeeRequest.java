package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.MemberId;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class PayMembershipFeeRequest {

    @NotNull
    int sum;

    @NotNull
    String memberId;

    @NotNull
    String clubId;

}
