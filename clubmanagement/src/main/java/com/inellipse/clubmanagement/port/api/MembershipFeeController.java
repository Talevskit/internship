package com.inellipse.clubmanagement.port.api;

import com.inellipse.clubmanagement.application.service.MembershipFeeService;
import com.inellipse.clubmanagement.domain.model.ClubMembership;
import com.inellipse.clubmanagement.domain.model.MembershipFee;
import com.inellipse.clubmanagement.port.requests.ClubMembershipCreateRequest;
import com.inellipse.clubmanagement.port.requests.MembershipFeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/api/membershipfees")
public class MembershipFeeController {

    private final MembershipFeeService membershipFeeService;

    @Autowired
    public MembershipFeeController(MembershipFeeService membershipFeeService) {
        this.membershipFeeService = membershipFeeService;
    }

    @GetMapping
    public List<MembershipFee> getMembershipFees(){return membershipFeeService.getMembershipFees();}

    @GetMapping("/{id}")
    public MembershipFee getMembershipFee(@PathVariable Long id) {
        return membershipFeeService.getMembershipFeeById(id);
    }

    @PostMapping
    public MembershipFee createMembershipFee (@RequestBody MembershipFeeRequest request) {
        return membershipFeeService.createMembershipFee(request);
    }

    @GetMapping("/paid")
    public List<MembershipFee> getMembershipsWhoPaid(){
        return membershipFeeService.getByWhoPaid();
    }
}
