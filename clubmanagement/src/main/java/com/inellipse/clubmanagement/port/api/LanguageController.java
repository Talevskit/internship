package com.inellipse.clubmanagement.port.api;

import com.inellipse.clubmanagement.application.service.LanguageService;
import com.inellipse.clubmanagement.domain.model.Language;
import com.inellipse.clubmanagement.port.requests.LanguageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping(path = "api/languages")
public class LanguageController {

    private final LanguageService languageService;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping
    public List<Language> getLanguages(){return this.languageService.getLanguages();}

    @PostMapping
    public Language createNewLanguage(@RequestBody LanguageRequest request){
        return languageService.createLanguage(request);
    }

}