package com.inellipse.clubmanagement.port.requests;

import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.MemberId;

import lombok.Data;
@Data
public class RemoveMembershipRequest {
	
	private ClubId clubId;
	private MemberId memberId;

}
