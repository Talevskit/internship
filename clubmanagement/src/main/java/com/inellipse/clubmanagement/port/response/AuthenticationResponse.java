package com.inellipse.clubmanagement.port.response;

import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.RefreshToken;
import lombok.Getter;


import java.io.Serializable;

@Getter
public class AuthenticationResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;

    private Member member;

    private final String token;

    private final RefreshToken refreshToken;

    private Long tokenExpirationDate;

    public AuthenticationResponse(Member member, String token, RefreshToken refreshToken, Long tokenExpirationDate) {
        this.member = member;
        this.token = token;
        this.refreshToken = refreshToken;
        this.tokenExpirationDate = tokenExpirationDate;
    }

    public AuthenticationResponse(String token, RefreshToken refreshToken){
        this.token = token;
        this.refreshToken = refreshToken;
    }
}
