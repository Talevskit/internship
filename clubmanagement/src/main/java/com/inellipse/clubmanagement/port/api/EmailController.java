package com.inellipse.clubmanagement.port.api;


import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inellipse.clubmanagement.application.serviceimpl.EmailServiceImpl;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.port.requests.EmailRequest;
import com.inellipse.clubmanagement.port.requests.GetEmailRequest;
import com.inellipse.clubmanagement.port.requests.SetNewPasswordRequest;
import com.sendgrid.Response;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class EmailController {
	
	@Autowired
	private EmailServiceImpl emailServiceImpl;

	@Autowired
	private MemberRepository memberRepository;
	
	@PostMapping("/sendEmail")
	public ResponseEntity<String> sendEmail(@RequestBody EmailRequest emailRequest) throws MessagingException, IOException {
		Response response = emailServiceImpl.sendEmail(emailRequest);
		if(response.getStatusCode() == 200 || response.getStatusCode() == 202)
			return new ResponseEntity<>("Email sent successfully", HttpStatus.OK);
		return new ResponseEntity<>("Failed to send", HttpStatus.NOT_FOUND);
	}

	@PostMapping("/resetPassword")
	public void resetPassword(@RequestBody GetEmailRequest email) throws MessagingException, IOException {
		try {
			emailServiceImpl.resetPassword(email);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@PostMapping("/reset")
	public String setNewPassword(@RequestBody SetNewPasswordRequest request) throws Exception {
		return emailServiceImpl.setNewPassword(request.getToken(), request.getPassword());
	}

	@GetMapping("/sendpdf/{id}")
	public String sendEmailWithPdf(@PathVariable String id) throws IOException, MessagingException {
		return emailServiceImpl.mailWithPdf(new MemberId(id));
	}

	@GetMapping("/{id}/verifyEmail")
	public void verifyEmail(@PathVariable String id) throws IOException, MessagingException {
		Member member = memberRepository.findById(new MemberId(id)).get();
		emailServiceImpl.confirmRegistration(member);

	}

	@GetMapping("/verify/token/{token}")
	public String setConfirmation(@PathVariable String token) throws Exception {
		return emailServiceImpl.setConfirmEmail(token);
	}

}
