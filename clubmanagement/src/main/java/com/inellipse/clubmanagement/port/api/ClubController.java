package com.inellipse.clubmanagement.port.api;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.inellipse.clubmanagement.domain.model.DistrictId;
import com.inellipse.clubmanagement.domain.model.UnionId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.inellipse.clubmanagement.application.service.ClubService;
import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.port.requests.ClubCreateRequest;
import com.inellipse.clubmanagement.port.response.ClubMembers;

@CrossOrigin
@RestController
@RequestMapping("/api/clubs")
public class ClubController {

    private final ClubService clubService;

    public ClubController(ClubService clubService) {
        this.clubService = clubService;
    }

    @GetMapping
    public List<ClubMembers> getClubs() {
        return clubService.getClubs();
    }

    @GetMapping("/{id}")
    public ClubMembers getClub(@PathVariable ClubId id) {
        return clubService.getClub(id);
    }

    @PostMapping
    public Club createNewClub (@RequestBody ClubCreateRequest request) {
        return clubService.createClub(request);
    }

    @DeleteMapping("/{id}")
    public void deleteClub (@PathVariable ClubId id){
        clubService.deleteByClubId(id);
    }

    @PatchMapping("/{idClub}")
    public Club editClub(@PathVariable String idClub,
                         @RequestParam("name") String name,
                         @RequestParam(value = "logo") String logo,
                         @RequestParam(value = "email") String email,
                         @RequestParam(value = "phone") Long phone,
                         @RequestParam(value = "districtId") DistrictId districtId,
                         @RequestParam(value = "unionId") UnionId unionId,
                         @RequestParam(value = "website") String website,
                         @RequestParam(value = "address") String address,
                         @RequestParam(value = "meetingTime") Date meetingTime,
                         @RequestParam(value = "meetingPlace") String meetingPlace,
                         @RequestParam (value = "price") long price)
            {
               Club c = new Club(new ClubId(idClub),name,logo,email,phone,districtId,unionId,website,
                                address,meetingTime,meetingPlace,price);
        return clubService.editClub(c,new ClubId(idClub));
    }

    @GetMapping("/districts/{districtId}")
    public List<Club> getClubsByDistrict(@PathVariable Long districtId){
        return clubService.getClubsByDistrict(districtId);
    }




}
