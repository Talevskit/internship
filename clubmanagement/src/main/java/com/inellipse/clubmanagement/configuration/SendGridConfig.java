package com.inellipse.clubmanagement.configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sendgrid.SendGrid;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

@Configuration
public class SendGridConfig {
	
	@Value("${sendgrid.key}")
	private String key;
	
	@Bean
	public SendGrid getSendgrid() {
		return new SendGrid(key);
	}



}
