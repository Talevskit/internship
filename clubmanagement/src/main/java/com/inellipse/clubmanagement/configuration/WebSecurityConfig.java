package com.inellipse.clubmanagement.configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    @Autowired
    private UserDetailsService jwtUserDetailsService;
    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }
    

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
    	httpSecurity
        .cors().and()
        .csrf().disable()
        .authorizeRequests()

                .antMatchers(HttpMethod.GET, "/").permitAll()
                .antMatchers(HttpMethod.GET, "/api/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/login").permitAll()
                .antMatchers(HttpMethod.POST, "/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/sendEmail").permitAll()
                .antMatchers(HttpMethod.GET, "/api/members/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/verify/token/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/verifyEmail/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/members/").permitAll()
                .antMatchers(HttpMethod.POST, "/api/members/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/clubs/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/membershipfees").permitAll()
                .antMatchers(HttpMethod.GET, "/api/membershipfees/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/members/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/members").permitAll()
                .antMatchers(HttpMethod.POST, "/api/refreshtoken").permitAll()
                .antMatchers(HttpMethod.POST, "/api/clubs").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/clubs/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/clubs/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/logout").permitAll()
                .antMatchers(HttpMethod.POST, "/api/membershipfees").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/api/membershipfees").permitAll()
                .antMatchers(HttpMethod.POST, "/api/clubmemberships").hasAnyAuthority("ADMIN","OWNER")
                .antMatchers(HttpMethod.POST, "/api/clubmemberships").permitAll()
                .antMatchers(HttpMethod.GET, "/firebase/**").permitAll()
//              .antMatchers(HttpMethod.POST, "/api/clubmemberships").hasAnyAuthority("ADMIN", "OWNER")
        .anyRequest().authenticated().and()
                .exceptionHandling()
        .authenticationEntryPoint(jwtAuthenticationEntryPoint)
        .and().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}