package com.inellipse.clubmanagement.exception;
public class ApiNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ApiNotFoundException(String message) {
        super(message);
    }

}
