package com.inellipse.clubmanagement.exception;

import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler{
	@ExceptionHandler(value = {Exception.class})
	public final ResponseEntity<Object> handleApiException(Exception exception) {

		HttpStatus badRequest = HttpStatus.BAD_REQUEST;
		CustomException badRequestException = new CustomException(exception.getLocalizedMessage(), badRequest,
				LocalDateTime.now());

		return new ResponseEntity<>(badRequestException, badRequest);
	}

	@ExceptionHandler(value = {ApiNotFoundException.class})
	public  ResponseEntity<Object> handleNotFoundException(ApiNotFoundException apiNotFoundException) {

		HttpStatus notFound = HttpStatus.NOT_FOUND;
		CustomException notFoundException = new CustomException(apiNotFoundException.getMessage(), notFound,
				LocalDateTime.now());

		return new ResponseEntity<>(notFoundException, notFound);
	}

}