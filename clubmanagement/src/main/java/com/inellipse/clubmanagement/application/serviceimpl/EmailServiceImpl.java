package com.inellipse.clubmanagement.application.serviceimpl;

import java.io.*;
import java.net.http.HttpRequest;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

import com.inellipse.clubmanagement.application.service.MemberService;
import com.inellipse.clubmanagement.domain.authentication.EmailConfirmation;
import com.inellipse.clubmanagement.domain.authentication.PasswordResetToken;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.inellipse.clubmanagement.domain.repository.ConfirmationTokenRepository;
import com.inellipse.clubmanagement.domain.repository.VerifyEmailRepository;
import com.inellipse.clubmanagement.port.requests.GetEmailRequest;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.sendgrid.helpers.mail.objects.Attachments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.inellipse.clubmanagement.domain.model.EmailDto;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.exception.ApiException;
import com.inellipse.clubmanagement.port.requests.EmailRequest;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;

@Service
public class EmailServiceImpl {

    @Autowired
    private SendGrid sendGrid;

    @Autowired
    private MembershipFeeServiceImpl membershipFeeServiceImpl;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private ConfirmationTokenRepository tokenRepository;

    @Autowired
    private MemberService memberService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private VerifyEmailRepository verifyEmailRepository;

    public Response sendEmail(EmailRequest emailRequest) throws MessagingException, IOException {
        Mail mail = new Mail(new Email("clubs.operator@gmail.com"), emailRequest.getSubject(),
                new Email(emailRequest.getTo()), new Content("text/plain", emailRequest.getBody()));
        mail.setReplyTo(new Email("clubs.operator@gmail.com"));
        Request request = new Request();
        Response response = null;
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            response = this.sendGrid.api(request);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return response;
    }


    public Response sendEmailWithAttachemnt(EmailRequest emailRequest, MemberId memberId) throws MessagingException, IOException {
        Member member = memberRepository.findById(memberId).get();
        Mail mail = new Mail(new Email("clubs.operator@gmail.com"), emailRequest.getSubject(),
                new Email(emailRequest.getTo()), new Content("text/plain", emailRequest.getBody()));
        mail.setReplyTo(new Email("clubs.operator@gmail.com"));

        addAttachment(mail, member);

        Request request = new Request();
        Response response = null;
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            response = this.sendGrid.api(request);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return response;
    }

    // @Scheduled(cron = "0 0/1 * 1/1 * ?")
    @Scheduled(cron = "0 0 9 20 1/1 ?")
    public String oweMoney() {
        membershipFeeServiceImpl.getMembershipFees().stream().forEach(x -> {
            if (x.getAmount() > x.getAmountPaid()) {
                Member member = memberRepository.findById(x.getMemberId()).orElseThrow(() -> {
                    return new ApiException("Member not found");
                });
                String gender = member.getGender().equals("male") ? "Mr" : "Ms";
                EmailRequest email = new EmailRequest(member.getEmail()
                        , "Club payment", "Hello " + gender + " " + member.getName()
                        + ",\n\nI hope you are enjoying the club activities you are attending.I think your feedback was terrific and really improved the final design.It's been a pleasure working with you!\n\nI'm attaching an invoice for the project."
                        + "As discussed, the total bill is $" + (x.getAmount() - x.getAmountPaid())
                        + ", payable via Paypal or via check.\nPlease let me know if you have any questions!\nThanks, and I look forward to working together in the future.\n\n Warm regards, Customer support team."
                );

                try {
                    sendEmail(email);
                } catch (MessagingException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return "Mail sent successfully";
    }

    public String resetPassword(GetEmailRequest emailMember) throws MessagingException, IOException {
        Optional<Member> newMember = memberRepository.findByEmail(emailMember.getEmail());

        PasswordResetToken passwordResetToken = new PasswordResetToken(newMember.get().id());

        passwordResetToken.setToken(UUID.randomUUID().toString());

        tokenRepository.save(passwordResetToken);

        String appUrl = "http://localhost:8080/api";

        EmailRequest email = new EmailRequest(emailMember.getEmail(), "Reset Password",
                "To reset your password, click the link below: " + appUrl + "/reset?token="
                        + passwordResetToken.getToken());

        sendEmail(email);

        return appUrl + "/reset?token=" + passwordResetToken.getToken();
    }

    public String setNewPassword(String token, String password) throws Exception {
        PasswordResetToken resetToken = tokenRepository.findByToken(token);

        Optional<Member> member = memberRepository.findById(resetToken.getMemberId());

        if (member.isPresent()) {
            Member resetMember = member.get();
            resetMember.setPassword(bcryptEncoder.encode(password));
            tokenRepository.save(resetToken);
            memberRepository.save(resetMember);
        } else {
            throw new Exception("Member Not Found");
        }
        return "Password Changed";
    }


    void addAttachment(Mail mail, Member member) throws IOException {
        byte[] out = memberService.pdfCreation(member);
        try (final InputStream inputStream = new ByteArrayInputStream(out)) {
            final Attachments attachments = new Attachments
                    .Builder(member.getName() + ".pdf", inputStream)
                    .withType("application/pdf")
                    .build();
            mail.addAttachments(attachments);
        }
    }


    public String mailWithPdf(MemberId member) throws IOException, MessagingException {
        Optional<Member> m = memberRepository.findById(member);
        EmailRequest email = new EmailRequest(m.get().getEmail(), "Payment", "File");
        sendEmailWithAttachemnt(email, member);
        return "Email send";
    }

    public void confirmRegistration(Member member) throws MessagingException, IOException {
        EmailConfirmation emailConfirmation = new EmailConfirmation(member.id());
        emailConfirmation.setToken(UUID.randomUUID().toString());
        verifyEmailRepository.save(emailConfirmation);
        String appUrl = "http://localhost:8080/api";
        EmailRequest email = new EmailRequest(member.getEmail(), "Confirm email",
                "To confirm your email " + member.getEmail() + ", click the link below: " + appUrl + "/verify/token/"
                        + emailConfirmation.getToken());

        sendEmail(email);

    }

    public String setConfirmEmail(String token) throws Exception {
        EmailConfirmation emailConfirmation = verifyEmailRepository.findByToken(token);

        Optional<Member> member = memberRepository.findById(emailConfirmation.getMemberId());

        if (member.isPresent()) {
            Member resetMember = member.get();
            resetMember.setEnabled(true);
            verifyEmailRepository.save(emailConfirmation);
            memberRepository.save(resetMember);
        } else {
            throw new Exception("Member Not Found");
        }
        return "Email Verified";
    }
}
