package com.inellipse.clubmanagement.application.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberDto;
import com.inellipse.clubmanagement.port.requests.MemberRequest;
import com.inellipse.clubmanagement.port.response.MemberResponse;

import javax.mail.MessagingException;

public interface MemberService {

	List<MemberDto> getMembers();

	MemberResponse createMember(MemberRequest request) throws IllegalStateException, IOException;
	
	Boolean deleteById(String id);

	MemberDto getById(String id);

	Member loadUserByUsername(Username valueOf);

	Optional<Member> getByEmail(String email);

	public Member updateMember(String id, MemberRequest request);

	byte[] pdfCreation(Member member);

	void payMembershipFee(int sum, String id, String clubId);

	Member lottery() throws MessagingException, IOException;

}
