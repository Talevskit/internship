package com.inellipse.clubmanagement.application.serviceimpl;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import com.inellipse.clubmanagement.exception.*;
import com.inellipse.clubmanagement.port.requests.EmailRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inellipse.clubmanagement.application.service.MemberService;
import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberDto;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.inellipse.clubmanagement.domain.model.MembershipFee;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.domain.repository.MembershipFeeRepository;
import com.inellipse.clubmanagement.domain.repository.RoleRepository;
import com.inellipse.clubmanagement.port.requests.MemberRequest;
import com.inellipse.clubmanagement.port.response.MemberResponse;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class MemberServiceImpl implements MemberService {
    private static final Logger logger = LoggerFactory.getLogger(MemberServiceImpl.class);


    private final MemberRepository memberRepository;

    private final PasswordEncoder bcryptEncoder;

    @Autowired
    private FileServiceImpl fileServiceImpl;

    @Autowired
    private MembershipFeeRepository membershipFeeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    public MemberServiceImpl(MemberRepository memberRepository, PasswordEncoder bcryptEncoder) {
        this.memberRepository = memberRepository;
        this.bcryptEncoder = bcryptEncoder;
    }

    @Override
    public List<MemberDto> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberDto> dtoMembers = new ArrayList<MemberDto>();
        members.stream().forEach(member -> {
            MemberDto dto = new MemberDto();
            MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
                    member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(), member.getCompany(),
                    member.getPhone(), member.getRoles(), member.getClubMemberships(), member.getMembershipFee());
            dto.setMember(mr);
            dto.setId(member.id());
            dtoMembers.add(dto);
        });
        return dtoMembers;
    }

    private String hashIt(String password) {
        String hashedPassword = null;
        int i = 0;
        while (i < 10) {
            hashedPassword = bcryptEncoder.encode(password);
            System.out.println(hashedPassword);
            i++;
        }
        return hashedPassword;
    }


//	@Override
//	public MemberResponse createMember(String member, MultipartFile file) throws IllegalStateException, IOException {
//		MemberRequest request = new MemberRequest();
//		ObjectMapper objMapper = new ObjectMapper();
//		try {
//			request=objMapper.readValue(member, MemberRequest.class);
//		} catch (JsonMappingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (JsonProcessingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		if (memberRepository.existsByUsername(request.getUsername())) {
//			throw new ApiException("Username already exists!");
//		}
//		if (memberRepository.existsByEmail(request.getEmail())) {
//			throw new ApiException("Email already exists!");
//		}
//		String pass = this.hashIt(request.getPassword());
//		Role role = roleRepository.findByName("MEMBER");
//		Member newMember = new Member(new MemberId(UUID.randomUUID().toString().replace("-", "")), request.getName(),
//				request.getSurname(), request.getAge(), request.getEmail(), request.getPhone(), request.getGender(),
//				request.getOccupation(), request.getCompany(), request.getUsername(), pass, request.isEnabled());
//		newMember.addRole(role);
//		memberRepository.saveAndFlush(newMember);
//
//			fileServiceImpl.fileUpload(file, newMember.id().getId());
//
//		MemberResponse response = new MemberResponse();
//		response.setName(request.getName());
//		response.setSurname(request.getSurname());
//		response.setUsername(request.getUsername());
//		response.setEmail(request.getEmail());
//		response.setAge(request.getAge());
//		response.setGender(request.getGender());
//		response.setOccupation(request.getOccupation());
//		response.setCompany(request.getCompany());
//		response.setMembershipFee(request.getMembershipFee());
//		response.setClubMemberships(request.getClubMemberships());
//		response.setRoles(request.getRoles());
//		return response;
//
//	}


    @Transactional
    @Override
    public Boolean deleteById(String id) {
        memberRepository.delete(memberRepository.findById(new MemberId(id)).orElseThrow(() -> {
            logger.error("Member not found");
            return new ApiNotFoundException("Member with id = " + id + " not found");
        }));
        return true;
    }

    @Override
    public MemberDto getById(String id) {
        Member member = memberRepository.findById(new MemberId(id)).orElseThrow(() -> {
            logger.error("Member not found");
            return new ApiNotFoundException("Member with id = " + id + " not found");
        });
        MemberDto dto = new MemberDto();
        MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
                member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(), member.getCompany(),
                member.getPhone(), member.getRoles(), member.getClubMemberships(), member.getMembershipFee());
        dto.setMember(mr);
        dto.setId(new MemberId(id));
        return dto;

    }

    @Override
    public Member loadUserByUsername(Username valueOf) {
        try {
            if (!memberRepository.existsByUsername(valueOf)) {
                logger.error("Member not found");
                throw  new ApiException("Member with username = " + valueOf.getValue() + " not found");
            }
            return memberRepository.findByUsername(valueOf);

        } catch (ApiException e) {
            Sentry.captureException(e);
        }
        return null;
    }

    @Override
    public Optional<Member> getByEmail(String email) {
            if (!memberRepository.existsByEmail(email)) {
                logger.error("Member not found");
                throw  new ApiException("Member with email = " + email + " not found");
            }
            return memberRepository.findByEmail(email);

    }

    @Override
    public Member updateMember(String id, MemberRequest request) {
        Member member = memberRepository.findById(new MemberId(id)).orElseThrow(() -> {
            logger.error("Member not found");
            return new ApiNotFoundException("Member with id = " + id + " not found");
        });
        member.setName(request.getName());
        member.setSurname(request.getSurname());
        member.setAge(request.getAge());
        member.setEmail(request.getEmail());
        member.setPhone(request.getPhone());
        member.setGender(request.getGender());
        member.setOccupation(request.getOccupation());
        member.setCompany(request.getCompany());
        member.setUsername(request.getUsername());
        return memberRepository.save(member);
    }

    @Override
    public MemberResponse createMember(MemberRequest request) {
        if (memberRepository.existsByUsername(request.getUsername())) {
            logger.error("Username already exists!");
            throw new ApiException("Username already exists!");
        }
        if (memberRepository.existsByEmail(request.getEmail())) {
            logger.error("Email already exists!");
            throw new ApiException("Email already exists!");
        }
        String pass = this.hashIt(request.getPassword());
        Role role = roleRepository.findByName("MEMBER");
        Member newMember = new Member(new MemberId(UUID.randomUUID().toString().replace("-", "")), request.getName(),
                request.getSurname(), request.getAge(), request.getEmail(), request.getPhone(),
                request.getGender(), request.getOccupation(), request.getCompany(), request.getUsername(),
                pass, request.isEnabled());
        newMember.addRole(role);
        memberRepository.save(newMember);
        MemberResponse response = new MemberResponse();
        response.setName(request.getName());
        response.setSurname(request.getSurname());
        response.setUsername(request.getUsername());
        response.setEmail(request.getEmail());
        response.setAge(request.getAge());
        response.setGender(request.getGender());
        response.setOccupation(request.getOccupation());
        response.setCompany(request.getCompany());
        response.setMembershipFee(request.getMembershipFee());
        response.setClubMemberships(request.getClubMemberships());
        response.setRoles(request.getRoles());
        return response;

    }

    @Override
    public byte[] pdfCreation(Member member) {
        Member member1 = memberRepository.findById(new MemberId(member.id().getId())).orElseThrow(() -> {
            logger.error("Member not found");
            return new ApiNotFoundException("Member with id = " + member.id().getId() + " not found");
        });
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            MembershipFee mf = membershipFeeRepository.findByMemberId(member.id());
            String gender = member.getGender().equals("male") ? "Mr" : "Ms";
            Document document = new Document();
            try {
                PdfWriter.getInstance(document, out);
                document.open();
                Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
                document.add(new Chunk("Club payment invoice\n\n", font));
                document.add(new Paragraph("Dear " + gender + " " + member.getName()
                        + "\nYour total amount to pay for the following month is $"
                        + (mf.getAmount() - mf.getAmountPaid())));
                document.close();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            return out.toByteArray();

    }

    @Override
    public void payMembershipFee(int sum, String id, String clubId) {
        Member member = memberRepository.findById(new MemberId(id)).orElseThrow(() -> {
            logger.error("Member not found");
            return new ApiNotFoundException("Member with id = " + id + " not found");
        });
        MembershipFee mf = membershipFeeRepository.findByMemberId(member.id());
        if(mf.getClubId().getId().equals(clubId)) {
            mf.setAmountPaid(mf.getAmountPaid() + sum);
            if (mf.getAmount() == mf.getAmountPaid()) {
                mf.setPaid(true);
            }
            membershipFeeRepository.save(mf);
        }
    }

    @Override
    @Scheduled(cron = "0 0 9 01 1/1 ?")
    public Member lottery() throws MessagingException, IOException {
        Member member = memberRepository.findByUsername(memberRepository.getMember());
        String gender = member.getGender().equals("male") ? "Mr" : "Ms";
        EmailRequest email = new EmailRequest(member.getEmail()
                , "Discount -50%", "Dear " + gender + " " + member.getName()
                + ",\n\nI hope you are enjoying the club activities you are attending. " +
                "You have won our monthly discount up to -50% for club membership by your choice"
                + "\nPlease let me know if you have any questions!\nThanks, and I look forward to working together in the future." +
                "\n\n Warm regards, Customer support team."
        );
        emailService.sendEmail(email);
        return member;
    }


}
