package com.inellipse.clubmanagement.application.serviceimpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inellipse.clubmanagement.application.service.ClubMembershipService;
import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.ClubMembership;
import com.inellipse.clubmanagement.domain.model.ClubMembershipsDto;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MembershipFee;
import com.inellipse.clubmanagement.domain.repository.ClubMembershipRepository;
import com.inellipse.clubmanagement.domain.repository.ClubRepository;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.domain.repository.MembershipFeeRepository;
import com.inellipse.clubmanagement.port.requests.ClubMembershipCreateRequest;
import com.inellipse.clubmanagement.port.response.MemberResponse;

@Service
public class ClubMembershipServiceImpl implements ClubMembershipService {

	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private ClubRepository clubRepository;
	@Autowired
	private MembershipFeeRepository membershipFeeRepository;

	private final ClubMembershipRepository clubMembershipRepository;

	public ClubMembershipServiceImpl(ClubMembershipRepository clubMembershipRepository) {
		this.clubMembershipRepository = clubMembershipRepository;
	}

	@Override
	public List<ClubMembershipsDto> getClubMemberships() {
		List<ClubMembershipsDto> memberships = new ArrayList<ClubMembershipsDto>();
		clubMembershipRepository.findAll().stream().forEach(x -> {
			ClubMembershipsDto membership = new ClubMembershipsDto();
			Member member = memberRepository.findById(x.getMemberId()).get();
			Club club = clubRepository.findClubById(x.getClubId());
			MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
					member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(), member.getCompany(),
					member.getPhone(), member.getRoles(), member.getClubMemberships(), member.getMembershipFee());
			membership.setMembershipId(x.getId());
			membership.setClub(club);
			membership.setMemberResponse(mr);
			memberships.add(membership);
		});
		return memberships;
	}

	@Override
	public ClubMembership createClubMembership(ClubMembershipCreateRequest request) {
		ClubMembership clubMembership = new ClubMembership(request.getClubId(), request.getMemberId(), request.getTo());
		clubMembership = clubMembershipRepository.saveAndFlush(clubMembership);
		clubMembership = clubMembershipRepository.save(clubMembership);
		return clubMembership;

	}

	@Override
	public ClubMembershipsDto getClubMembership(Long id) {
		ClubMembership cm = clubMembershipRepository.findById(id);
		ClubMembershipsDto membership = new ClubMembershipsDto();
		Member member = memberRepository.findById(cm.getMemberId()).get();
		Club club = clubRepository.findClubById(cm.getClubId());
		
		MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
				member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(), member.getCompany(),
				member.getPhone(), member.getRoles(), member.getClubMemberships(), member.getMembershipFee());

		membership.setMembershipId(cm.getId());
		membership.setClub(club);
		membership.setMemberResponse(mr);
		return membership;
	}

	@Override
	public List<ClubMembershipsDto> getClubMembershipByClubId(ClubId clubId) {
		List<ClubMembershipsDto> memberships = new ArrayList<ClubMembershipsDto>();
		clubMembershipRepository.findAll().stream().forEach(x -> {
			if (x.getClubId().getId().equals(clubId.getId())) {
				ClubMembershipsDto membership = new ClubMembershipsDto();
				Member member = memberRepository.findById(x.getMemberId()).get();
				Club club = clubRepository.findClubById(x.getClubId());
				MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
						member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(),
						member.getCompany(), member.getPhone(), member.getRoles(), member.getClubMemberships(),
						member.getMembershipFee());
				membership.setMembershipId(x.getId());
				membership.setClub(club);
				membership.setMemberResponse(mr);
				memberships.add(membership);
			}
		});
		return memberships;
	}

	@Transactional
	@Override
	public void deleteByClubMembershipId(Long id) {
		clubMembershipRepository.deleteById(id);
	}
}
