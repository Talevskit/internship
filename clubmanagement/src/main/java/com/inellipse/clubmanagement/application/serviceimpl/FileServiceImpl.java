package com.inellipse.clubmanagement.application.serviceimpl;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl {

	public String fileUpload(MultipartFile file, String id) throws IllegalStateException, IOException {
		file.transferTo(new File(
				"C:\\Users\\Tino\\repo\\ClubManagementBackEnd\\clubmanagement\\src\\main\\resources\\images",
				id+".jpg"));
		return "Image uploaded succesfully";
	}
}
