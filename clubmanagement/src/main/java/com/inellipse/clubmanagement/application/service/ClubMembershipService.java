package com.inellipse.clubmanagement.application.service;

import java.util.List;

import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.ClubMembership;
import com.inellipse.clubmanagement.domain.model.ClubMembershipsDto;
import com.inellipse.clubmanagement.port.requests.ClubMembershipCreateRequest;


public interface ClubMembershipService {

    List<ClubMembershipsDto> getClubMemberships();

    ClubMembership createClubMembership(ClubMembershipCreateRequest request);

    ClubMembershipsDto getClubMembership(Long id);

    void deleteByClubMembershipId(Long id);

	List<ClubMembershipsDto> getClubMembershipByClubId(ClubId clubId);

}
