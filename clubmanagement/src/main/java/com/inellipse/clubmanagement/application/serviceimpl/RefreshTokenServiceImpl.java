package com.inellipse.clubmanagement.application.serviceimpl;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.inellipse.clubmanagement.domain.model.RefreshToken;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.domain.repository.RefreshTokenRepository;
import com.inellipse.clubmanagement.exception.TokenRefreshException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class RefreshTokenServiceImpl {

    private int refreshExpirationDateInMs;

    @Value("${jwt.refreshExpirationDateInMs}")
    public void setRefreshExpirationDateInMs(int refreshExpirationDateInMs) {
        this.refreshExpirationDateInMs = refreshExpirationDateInMs;
    }

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private MemberRepository memberRepository;

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(Member member) {
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setUser(member.id());
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshExpirationDateInMs));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenRefreshException(token.getToken(), "Refresh token has expired. Please make a new signin request");
        }

        return token;
    }

    @Transactional
    public void deleteByMemberId(MemberId memberId) {
        refreshTokenRepository.deleteByMemberId(memberId.getId());
    }
}
