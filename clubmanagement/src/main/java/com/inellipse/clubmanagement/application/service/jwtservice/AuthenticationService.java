package com.inellipse.clubmanagement.application.service.jwtservice;


import com.inellipse.clubmanagement.application.service.MemberService;
import com.inellipse.clubmanagement.application.serviceimpl.RefreshTokenServiceImpl;
import com.inellipse.clubmanagement.configuration.JwtTokenUtil;
import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.RefreshToken;
import com.inellipse.clubmanagement.port.response.AuthenticationResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService userDetailsService;
    private final MemberService memberService;
    private final RefreshTokenServiceImpl tokenService;
    public AuthenticationService(AuthenticationManager authenticationManager,
                                 JwtTokenUtil jwtTokenUtil,
                                 JwtUserDetailsService userDetailsService,
                                 MemberService memberService, RefreshTokenServiceImpl tokenService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.memberService = memberService;
        this.tokenService = tokenService;
    }

    public void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public AuthenticationResponse generateToken(String username) {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
        final Date tokenExpirationDate = jwtTokenUtil.getExpirationDateFromToken(token);
        final Member member = memberService.loadUserByUsername(Username.valueOf(username));
        final RefreshToken refreshToken = tokenService.createRefreshToken(member);
        return new AuthenticationResponse(member, token, refreshToken, tokenExpirationDate.getTime());
    }
}