package com.inellipse.clubmanagement.application.serviceimpl;

import com.inellipse.clubmanagement.application.service.LanguageService;
import com.inellipse.clubmanagement.domain.model.Language;
import com.inellipse.clubmanagement.domain.repository.LanguageRepository;
import com.inellipse.clubmanagement.port.requests.LanguageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepository languageRepository;

    public LanguageServiceImpl(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @Override
    public List<Language> getLanguages() {
        return languageRepository.findAll();
    }

    @Override
    public Language createLanguage(LanguageRequest request) {
        Language newLanguage = new Language(
                request.getId(),
                request.getLanguageName(),
                request.getMemberId());

        newLanguage = languageRepository.saveAndFlush(newLanguage);
        newLanguage = languageRepository.save(newLanguage);
        return  newLanguage;
    }
}
