package com.inellipse.clubmanagement.application.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.inellipse.clubmanagement.exception.ApiNotFoundException;
import com.inellipse.clubmanagement.exception.ResourceNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inellipse.clubmanagement.application.service.ClubMembershipService;
import com.inellipse.clubmanagement.application.service.ClubService;
import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberDto;
import com.inellipse.clubmanagement.domain.repository.ClubMembershipRepository;
import com.inellipse.clubmanagement.domain.repository.ClubRepository;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import com.inellipse.clubmanagement.exception.ApiException;
import com.inellipse.clubmanagement.port.requests.ClubCreateRequest;
import com.inellipse.clubmanagement.port.response.ClubMembers;
import com.inellipse.clubmanagement.port.response.MemberResponse;

@Service
public class ClubServiceImpl implements ClubService {

	private static final Logger logger = LoggerFactory.getLogger(MemberServiceImpl.class);
	
	@Autowired
	private ClubMembershipService clubMembershipService;

    private final ClubRepository clubRepository;

    public ClubServiceImpl(ClubRepository clubRepository) {
        this.clubRepository = clubRepository;
    }
    @Autowired
    private MemberRepository memberRepository;
    
    @Autowired
    private ClubMembershipRepository clubMembershipRepository;

    @Override
    public List<ClubMembers> getClubs() {
    	List<ClubMembers> clubs = new ArrayList<ClubMembers>();
    	clubRepository.findAll().stream().forEach(x-> {
    		ClubMembers clubMembers = new ClubMembers();
    		List<MemberDto> members  = new ArrayList<MemberDto>();
    		clubMembershipRepository.findAll().stream().forEach(y->{
    			if(x.id().getId().equals(y.getClubId().getId())) {
    				MemberDto dto = new MemberDto();
    				Member member = memberRepository.findById(y.getMemberId()).get();
    				MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
        					member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(),
        					member.getCompany(), member.getPhone(), member.getRoles(), member.getClubMemberships(),
        					member.getMembershipFee());
    				dto.setId(member.id());
    				dto.setMember(mr);
    				members.add(dto);
    			}
    		});
    		clubMembers.setId(x.id());
    		clubMembers.setClub(x);
    		clubMembers.setMembers(members);
    		clubs.add(clubMembers);
    	});
    	return clubs;
    }

    @Override

    public Club createClub(ClubCreateRequest request) {
		if (clubRepository.existsByName(request.getName())) {
			logger.debug(String.format("Club with name %s already exist!", request.getName()));
			throw new ApiException(String.format("Club with name %s already exist!", request.getName()));
		}
		Club newClub = new Club(
				new ClubId(UUID.randomUUID().toString().replace("-", "")),
				request.getName(),
				request.getLogo(),
				request.getEmail(),
				request.getPhone(),
				request.getDistrictId(),
				request.getUnionId(),
				request.getWebsite(),
				request.getAddress(),
				request.getMeetingTime(),
				request.getMeetingPlace(),request.getPrice());
		newClub = clubRepository.save(newClub);
		return newClub;
	}
        @Override
        public ClubMembers getClub(ClubId clubId) {
        	Club club = clubRepository.findById(clubId).orElseThrow(() -> {
        		logger.info("Club not found");
        		return new ApiNotFoundException("Club not found");
        	});
        	ClubMembers clubMembers = new ClubMembers();
    		List<MemberDto> members  = new ArrayList<MemberDto>();
        	clubMembershipRepository.findAll().stream().forEach(y->{
    			if(club.id().getId().equals(y.getClubId().getId())) {
    				MemberDto dto = new MemberDto();
    				Member member = memberRepository.findById(y.getMemberId()).get();
    				MemberResponse mr = new MemberResponse(member.getName(), member.getSurname(), member.getUsername(),
        					member.getEmail(), member.getGender(), member.getAge(), member.getOccupation(),
        					member.getCompany(), member.getPhone(), member.getRoles(), member.getClubMemberships(),
        					member.getMembershipFee());
    				dto.setId(member.id());
    				dto.setMember(mr);
    				members.add(dto);
    			}
    		});
        	clubMembers.setId(club.id());
    		clubMembers.setClub(club);
    		clubMembers.setMembers(members);
    		return clubMembers;
        }
    	

    @Transactional
    @Override
    public void deleteByClubId(ClubId id){
			Club club = clubRepository.findById(id).orElseThrow(() -> {
				logger.info("Club not found");
				return new ApiNotFoundException("Club not found");
			});
			clubRepository.delete(club);
		}

    @Override
    public Club editClub(Club club, ClubId clubId) {
        Club c = this.clubRepository.findClubById(clubId);
       c.setAddress(club.getAddress());
       c.setDistrictId(club.getDistrictId());
       c.setEmail(club.getEmail());
       c.setLogo(club.getLogo());
       c.setMeetingPlace(club.getMeetingPlace());
       c.setMeetingTime(club.getMeetingTime());
       c.setName(club.getName());
       c.setPhone(club.getPhone());
       c.setUnionId(club.getUnionId());
       c.setWebsite(club.getWebsite());
       return clubRepository.save(c);

    }

    @Override
    public List<Club> getClubsByDistrict(Long districtId) {
        return clubRepository.getClubsByDistrictId(districtId);
    }
}
