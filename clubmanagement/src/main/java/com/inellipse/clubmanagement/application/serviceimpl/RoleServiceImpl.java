package com.inellipse.clubmanagement.application.serviceimpl;

import com.inellipse.clubmanagement.application.service.RoleService;
import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.repository.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Role getRole(Long id) {
        return repository.findById(id);
    }
}
