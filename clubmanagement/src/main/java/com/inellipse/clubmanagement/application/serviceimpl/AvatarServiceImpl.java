package com.inellipse.clubmanagement.application.serviceimpl;
import com.inellipse.clubmanagement.domain.avatar.Avatar;
import com.inellipse.clubmanagement.domain.avatar.SquareAvatar;

import org.apache.pdfbox.io.IOUtils;
import org.springframework.stereotype.Service;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


@Service
public class AvatarServiceImpl {

    public byte [] createAvatar() throws IOException {
        Avatar avatar = SquareAvatar.newAvatarBuilder().build();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte [] out = avatar.createAsPngBytes(avatar.hashCode());
        ByteArrayInputStream bis = new ByteArrayInputStream(out);
        BufferedImage bImage = ImageIO.read(bis);
        ImageIO.write(bImage,"png",outputStream);
        return outputStream.toByteArray();
    }
    public byte[] getImage(String id) throws IOException {
        InputStream in = getClass()
          .getResourceAsStream("C:\\\\Users\\\\Tino\\\\repo\\\\ClubManagementBackEnd\\\\clubmanagement\\\\src\\\\main\\\\resources\\\\images\\\\\"+id+\".jpg");
        return IOUtils.toByteArray(in);
    }
    
    
}

