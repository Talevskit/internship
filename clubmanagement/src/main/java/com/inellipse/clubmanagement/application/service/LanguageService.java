package com.inellipse.clubmanagement.application.service;

import com.inellipse.clubmanagement.domain.model.Language;
import com.inellipse.clubmanagement.port.requests.LanguageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LanguageService {

    List<Language> getLanguages();

    Language createLanguage(LanguageRequest request);


}
