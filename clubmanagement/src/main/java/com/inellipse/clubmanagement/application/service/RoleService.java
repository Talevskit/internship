package com.inellipse.clubmanagement.application.service;

import com.inellipse.clubmanagement.domain.authentication.Role;

public interface RoleService {

    Role getRole(Long id);

}
