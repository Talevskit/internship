package com.inellipse.clubmanagement.application.serviceimpl;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.messaging.*;
import com.inellipse.clubmanagement.domain.firebase.FireBaseInformation;
import com.inellipse.clubmanagement.domain.model.FireBaseToken;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.inellipse.clubmanagement.domain.repository.FireBaseInformationRepository;
import com.inellipse.clubmanagement.domain.repository.FireBaseTokenRepository;
import com.inellipse.clubmanagement.port.requests.Note;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.json.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;

@Service
public class FirebaseMessagingService {

    @Autowired
    private final FirebaseMessaging firebaseMessaging;

    @Autowired
    private final FireBaseTokenRepository repository;

    @Autowired
    private final FireBaseInformationRepository fireBaseInformationRepository;

    public FirebaseMessagingService(FirebaseMessaging firebaseMessaging, FireBaseTokenRepository repository, FireBaseInformationRepository fireBaseInformationRepository) {
        this.firebaseMessaging = firebaseMessaging;
        this.repository = repository;
        this.fireBaseInformationRepository = fireBaseInformationRepository;
    }

    public String sendNotification(Note note, String memberId) throws FirebaseMessagingException, FirebaseAuthException, IOException {
        FireBaseToken fireBaseToken = repository.findByMember(new MemberId(memberId)).get();

        Notification notification = Notification
                .builder()
                .setTitle(note.getSubject())
                .setBody(note.getContent())
                .build();

        Message message = Message
                .builder().setApnsConfig(ApnsConfig.builder()
                        .putHeader("apns-priority", "5")
                        .setAps(Aps.builder()
                            .setContentAvailable(true)
                            .build())
                        .build())
                .setToken(fireBaseToken.getToken())
                .setNotification(notification)
                .build();

        return firebaseMessaging.send(message);
    }

    public String sendNotificationWithTopic(Note note, String topic) throws FirebaseMessagingException, FirebaseAuthException, IOException {

        Message message = Message.builder()
                .setNotification(Notification.builder()
                        .setTitle("$GOOG up 1.43% on the day")
                        .setBody("$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.")
                        .build())
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000)
                        .setNotification(AndroidNotification.builder()
                                .setIcon("stock_ticker_update")
                                .setColor("#f45342")
                                .build())
                        .build())
                .setApnsConfig(ApnsConfig.builder()
                        .putHeader("apns-priority", "10")
                        .setAps(Aps.builder()
                                .setAlert(ApsAlert.builder()
                                        .setTitle("$GOOG up 1.43% on the day")
                                        .setBody("$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.")
                                        .build())
                                .setBadge(42)
                                .build())
                        .build())
                .setTopic("sport")
                .build();

        return firebaseMessaging.send(message);
    }

    public String setToken(String token, String id) throws FirebaseAuthException, IOException {
        FireBaseToken fireBaseToken = new FireBaseToken(token);
        fireBaseToken.setMember(new MemberId(id));
        repository.save(fireBaseToken);
        return token;
    }

    public String retrieveData() throws IOException, ParseException, JSONException {
        //String firebaseToken = "f_Na3TdN5kDNo_b5DurigG:APA91bGDMEe-jQ9pJWtIkOf-cFo1erv-jBTmM_WFZOXJF6oii8E9KsJDojMIC0Z8tS96FfDeQyMgFw6F8ZNKweB4HQlWhXHerD9LAmiLcSPwH_0zYnNLRPV5GsrG9QtMIoW_yRmNHQig";
        String requestUrl = "https://iid.googleapis.com/iid/info/f_Na3TdN5kDNo_b5DurigG:APA91bGDMEe-jQ9pJWtIkOf-cFo1erv-jBTmM_WFZOXJF6oii8E9KsJDojMIC0Z8tS96FfDeQyMgFw6F8ZNKweB4HQlWhXHerD9LAmiLcSPwH_0zYnNLRPV5GsrG9QtMIoW_yRmNHQig";
        URL url = new URL(requestUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        String token = "key=AAAARzoU2M4:APA91bHuNt4TwN_wTq0I3jPgT8U54PCq2tHbK8VeuGGMzo3ML229McbH0WfSVb3ygxPfmpuaq9yE-rHEe20cxiiCKoGSA8dwORSp9ZDN226C1cjeMqjb5bxn-ADsp_APSVZrxAh-IoWB";
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", token);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.connect();
        String jsonResponse = null;
        InputStream inputStr = conn.getInputStream();
        String encoding = conn.getContentEncoding() == null ? "UTF-8"
                : conn.getContentEncoding();
        jsonResponse = IOUtils.toString(inputStr, encoding);
        ObjectMapper objectMapper = new ObjectMapper();
        FireBaseInformation fireBaseInformation = objectMapper.readValue(jsonResponse, FireBaseInformation.class);
        fireBaseInformationRepository.save(fireBaseInformation);
        System.out.println(fireBaseInformation.getPlatform());
        return jsonResponse;

    }
}