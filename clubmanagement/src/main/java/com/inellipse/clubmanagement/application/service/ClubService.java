package com.inellipse.clubmanagement.application.service;

import java.util.List;

import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import com.inellipse.clubmanagement.port.requests.ClubCreateRequest;
import com.inellipse.clubmanagement.port.requests.ClubMembershipCreateRequest;
import com.inellipse.clubmanagement.port.response.ClubMembers;


public interface ClubService {

    List<ClubMembers> getClubs();

    Club createClub(ClubCreateRequest request);

    ClubMembers getClub(ClubId clubId);

    void deleteByClubId(ClubId clubId);

    List<Club> getClubsByDistrict(Long districtId);

    public Club editClub(Club club, ClubId clubId);
}
