package com.inellipse.clubmanagement.application.service;

import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MembershipFee;
import com.inellipse.clubmanagement.port.requests.MembershipFeeRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MembershipFeeService {

    List<MembershipFee> getMembershipFees();

    MembershipFee getMembershipFeeById(Long id);

    MembershipFee createMembershipFee(MembershipFeeRequest request);

    List<MembershipFee> getByWhoPaid();
}