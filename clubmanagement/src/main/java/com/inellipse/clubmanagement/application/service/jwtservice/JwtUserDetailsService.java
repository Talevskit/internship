package com.inellipse.clubmanagement.application.service.jwtservice;

import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.repository.MemberRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final MemberRepository memberRepository;

    public JwtUserDetailsService(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member user = this.memberRepository.findByUsername(Username.valueOf(username));
                //.orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        return new MyUserDetails(user);
    }

    public Member loadUser(String username) {
        return memberRepository.findByUsername(Username.valueOf(username));
        //.orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
    }
}
