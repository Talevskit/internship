package com.inellipse.clubmanagement.application.serviceimpl;

import com.inellipse.clubmanagement.application.service.MembershipFeeService;
import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.MembershipFee;

import com.inellipse.clubmanagement.domain.repository.ClubRepository;
import com.inellipse.clubmanagement.domain.repository.MembershipFeeRepository;
import com.inellipse.clubmanagement.port.requests.MembershipFeeRequest;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MembershipFeeServiceImpl implements MembershipFeeService {

    private final MembershipFeeRepository membershipFeeRepository;

    private final ClubRepository clubRepository;

    public MembershipFeeServiceImpl(MembershipFeeRepository membershipFeeRepository, ClubRepository clubRepository) {
        this.membershipFeeRepository = membershipFeeRepository;
        this.clubRepository = clubRepository;
    }

    @Override
    public List<MembershipFee> getMembershipFees() {
        return membershipFeeRepository.findAll();
    }

    @Override
    public MembershipFee getMembershipFeeById(Long id) {
        return membershipFeeRepository.findById(id);
    }

    @Override
    public MembershipFee createMembershipFee(MembershipFeeRequest request) {
        Club club = clubRepository.findClubById(request.getClubId());
        MembershipFee membershipFee= new MembershipFee(
                request.getId(),
                club.getPrice(),
                request.getPaidOn(),
                request.getMemberId(),
                request.isPaid(),
                request.getAmountPaid(),
                request.getClubId()
        );
        membershipFee=membershipFeeRepository.saveAndFlush(membershipFee);
        membershipFee=membershipFeeRepository.save(membershipFee);
        return  membershipFee;
    }

    @Override
    public List<MembershipFee> getByWhoPaid() {
        return membershipFeeRepository.findMembershipFeesByPaidTrue();
    }

}
