package com.inellipse.clubmanagement.domain.model;

import com.inellipse.clubmanagement.domain.base.AbstractEntity;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Getter
@Table(name = "club_unions")
public class Union extends AbstractEntity<UnionId> {

    private String unionName;

    public Union(UnionId id, String name) {
        super(id);
        this.unionName = name;
    }

    @Override
    public UnionId id() {
        return id;
    }
}
