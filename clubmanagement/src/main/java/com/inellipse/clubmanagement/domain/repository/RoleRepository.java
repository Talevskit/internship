package com.inellipse.clubmanagement.domain.repository;


import com.inellipse.clubmanagement.domain.authentication.Role;
import com.sun.xml.bind.v2.model.core.ID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

    Role findByName(String name);

    Role findById(Long id);

}
