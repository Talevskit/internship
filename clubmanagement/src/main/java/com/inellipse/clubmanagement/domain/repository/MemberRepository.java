package com.inellipse.clubmanagement.domain.repository;

import java.util.Optional;

import com.inellipse.clubmanagement.domain.model.MemberDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberId;

@Repository
public interface MemberRepository extends JpaRepository<Member, MemberId> {

    Optional<Member> findByEmail(String email);

    Member findByUsername(Username username);

    Optional<Member> findById(MemberId id);

	Boolean existsByEmail(String email);
	
	Boolean existsByUsername(Username username);

	Boolean existsById(String id);

    @Query(value = "SELECT m.username FROM members m ORDER BY random() LIMIT 1", nativeQuery = true)
    Username getMember();
}