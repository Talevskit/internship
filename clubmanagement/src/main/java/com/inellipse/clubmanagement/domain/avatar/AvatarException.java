package com.inellipse.clubmanagement.domain.avatar;

public class AvatarException extends RuntimeException {

    public AvatarException(String message, Throwable cause) {
        super(message, cause);
    }

}
