package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.model.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends JpaRepository<Language,String> {

    Language findById(Long id);
}