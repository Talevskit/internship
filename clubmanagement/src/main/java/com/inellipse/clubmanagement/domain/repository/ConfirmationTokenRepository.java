package com.inellipse.clubmanagement.domain.repository;


import com.inellipse.clubmanagement.domain.authentication.PasswordResetToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationTokenRepository extends CrudRepository<PasswordResetToken, String> {
    PasswordResetToken findByToken(String confirmationToken);
}