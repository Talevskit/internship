package com.inellipse.clubmanagement.domain.model;

import com.inellipse.clubmanagement.domain.model.MemberId;
import lombok.Getter;
import org.attoparser.dom.Text;
import org.hibernate.annotations.Type;

import javax.persistence.*;


@Entity(name = "firebasetoken")
@Table(name = "firebase_token")
@Getter
public class FireBaseToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    MemberId member;

    @Column(nullable = false, unique = true)
    @Type(type = "text")
    private String token;

    private String platform;

    public FireBaseToken() { }

    public FireBaseToken(String token){
        this.token = token;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMember(MemberId member) {
        this.member = member;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
