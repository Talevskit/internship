package com.inellipse.clubmanagement.domain.model;

import java.time.ZoneOffset;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Data
@Entity
@Table(name = "clubmemberships")
public class ClubMembership {

    @Id
    @Column(name = "clubMembershipId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "club_id", nullable = false))})
    private ClubId clubId;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    MemberId memberId;

    @Column(name = "sinceDate")
    private Date since;

    @Column(name="toDate")
    private int to;
    
    @PrePersist
	public void init() {
		since = Date.from(java.time.ZonedDateTime.now(ZoneOffset.UTC).toInstant());
	}


    public ClubMembership(ClubId clubId,MemberId memberId, int to) {
        this.memberId = memberId;
        this.clubId = clubId;
        this.to = to;
    }

}
