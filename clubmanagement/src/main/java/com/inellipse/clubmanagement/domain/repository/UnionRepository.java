package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.model.Union;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnionRepository extends JpaRepository<Union, String> {

}
