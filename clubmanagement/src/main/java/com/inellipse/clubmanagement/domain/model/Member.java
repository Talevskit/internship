package com.inellipse.clubmanagement.domain.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inellipse.clubmanagement.domain.authentication.Role;
import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.base.AbstractEntity;

import  lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "members")
@NoArgsConstructor
public class Member  extends AbstractEntity<MemberId> {


     private String name;

     private String surname;

     private Integer age;

     private String email;

     private Long phone;

     @JsonIgnore
     @Transient
     @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
     private Set<Link> links;

     @JsonIgnore
     @Transient
     @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
     private Set<Language> languages;

     private String gender;

     private String occupation;

     private String company;

     @JsonIgnore
     @Transient
     @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
     Set<ClubMembership> clubMemberships;

     @JsonIgnore
     @Transient
     @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
     Set<MembershipFee> MembershipFee;

     @Enumerated
     @AttributeOverrides({@AttributeOverride(name = "value", column = @Column(name = "username", nullable = false))})
     Username username;
     
     private String password;

     @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
     private Set<Role> roles = new HashSet<Role>();

     @Column(name = "enabled")
     private boolean enabled;

     public Member(MemberId memberId, String name, String surname, Integer age,
                  String email, Long phone,
                  String gender, String occupation, String company,
                  Username username, String password, boolean enabled) {
        super(memberId);
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.occupation = occupation;
        this.company = company;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
     }



    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setUsername(Username username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean en){
         this.enabled = en;
    }

    @Override
    public MemberId id() {
        return id;
    }

    public Role addRole(Role role){
        roles.add(role);
        return role;
    }
}