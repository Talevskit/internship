package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.model.ClubMembership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ClubMembershipRepository extends JpaRepository<ClubMembership, String> {

    ClubMembership findById(Long id);

    void deleteById(Long id);
}
