package com.inellipse.clubmanagement.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "languages")
@NoArgsConstructor
public class Language {
    @Id
    @SequenceGenerator(name="language_sequence",sequenceName = "language_sequence",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "language_sequence")
    private Long id;

    private String languageName;

    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    @Embedded
    private MemberId memberId;

    public Language(Long id,String languageName, MemberId memberId) {
        this.id = id;
        this.languageName = languageName;
        this.memberId=memberId;
    }

}
