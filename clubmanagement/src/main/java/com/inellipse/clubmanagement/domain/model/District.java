package com.inellipse.clubmanagement.domain.model;

import com.inellipse.clubmanagement.domain.base.AbstractEntity;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
@Getter
public class District extends AbstractEntity<DistrictId> {

    private String districtName;

    public District(DistrictId id, String name){
        super(id);
        this.districtName = name;
    }

    @Override
    public DistrictId id() {
        return id;
    }
}
