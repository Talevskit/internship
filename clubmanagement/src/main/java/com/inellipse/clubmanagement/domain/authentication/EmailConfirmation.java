package com.inellipse.clubmanagement.domain.authentication;

import com.inellipse.clubmanagement.domain.model.MemberId;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Entity
@NoArgsConstructor
@Getter
@Data
public class EmailConfirmation {
    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    private MemberId memberId;

    private Date sendDate;

    public EmailConfirmation(MemberId memberId) {
        this.memberId = memberId;
    }

    /*private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }*/


}
