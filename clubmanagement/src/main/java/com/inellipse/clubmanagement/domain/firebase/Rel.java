package com.inellipse.clubmanagement.domain.firebase;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
public class Rel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

    @JsonProperty("topics")
    @AttributeOverrides({@AttributeOverride(name = "topic-name", column = @Column(name = "topic", nullable = false))})
    private Topics topics;

    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonIgnore
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH})
	@JoinColumn(name="info_id")
    private FireBaseInformation firebaseInfo;
    
    @JsonProperty("topics")
    public Topics getTopics() {
        return topics;
    }

    @JsonProperty("topics")
    public void setTopics(Topics topics) {
        this.topics = topics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}