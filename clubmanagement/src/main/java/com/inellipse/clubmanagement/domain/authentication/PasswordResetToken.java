package com.inellipse.clubmanagement.domain.authentication;

import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberId;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@Getter
public class PasswordResetToken {

    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    private MemberId memberId;

    private Date expiryDate;

    public PasswordResetToken(MemberId memberId) {
        this.memberId = memberId;
    }


    public void setToken(String token) {
        this.token = token;
    }
}
