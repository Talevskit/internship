package com.inellipse.clubmanagement.domain.model;

import com.inellipse.clubmanagement.port.response.MemberResponse;

import lombok.Data;
@Data
public class ClubMembershipsDto {
	
	private Long membershipId;
	private Club club;
	private MemberResponse memberResponse;

}
