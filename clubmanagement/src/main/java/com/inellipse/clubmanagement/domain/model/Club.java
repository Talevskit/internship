package com.inellipse.clubmanagement.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inellipse.clubmanagement.domain.base.AbstractEntity;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@NoArgsConstructor
@Data
@Entity
@Table(name = "clubs")
public class Club extends AbstractEntity<ClubId> {

    @Column(name = "clubName")
    @NotNull
    private String name;

    @Column(name = "clubLogo")
    private String logo;

    @Column(name = "clubEmail")
    private String email;

    @Column(name = "clubPhone")
    private long phone;

    @JsonIgnore
    @Transient
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    private Set<MembershipFee> clubMembershipFees;

    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "district_id", nullable = false))})
    @Embedded
    private DistrictId districtId;

    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "union_id", nullable = false))})
    @Embedded
    private UnionId unionId;

    private String website;

    @Column(name = "clubAddress")
    private String address;

    @JsonFormat(pattern="dd/MM/20yy-HH:mm")
    private Date meetingTime;

    private String meetingPlace;

    @JsonIgnore
    @Transient
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    private Set<ClubMembership> clubMemberships;

    private long price;

    public Club(ClubId clubid,String name, String logo, String email, long phone, DistrictId districtId, UnionId unionId, String website, String address, Date meetingTime, String meetingPlace,long price) {
        super(clubid);
        this.name = name;
        this.logo = logo;
        this.email = email;
        this.phone = phone;
        this.districtId = districtId;
        this.unionId = unionId;
        this.website = website;
        this.address = address;
        this.meetingTime = meetingTime;
        this.meetingPlace = meetingPlace;
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public void setDistrictId(DistrictId districtId) {
        this.districtId = districtId;
    }

    public void setUnionId(UnionId unionId) {
        this.unionId = unionId;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMeetingTime(Date meetingTime) {
        this.meetingTime = meetingTime;
    }

    public void setMeetingPlace(String meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    public void setPrice(long price){
        this.price = price;
    }

    @Override
    public ClubId id() {
        return id;
    }
}

