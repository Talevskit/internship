package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.model.Member;
import com.inellipse.clubmanagement.domain.model.MemberId;
import com.inellipse.clubmanagement.domain.model.MembershipFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MembershipFeeRepository extends JpaRepository<MembershipFee,String> {

    MembershipFee findById(Long id);

    void deleteById(Long id);

    List<MembershipFee> findMembershipFeesByPaidTrue();

    MembershipFee findByMemberId(MemberId m);
}
