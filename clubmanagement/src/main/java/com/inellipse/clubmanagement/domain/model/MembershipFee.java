package com.inellipse.clubmanagement.domain.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;

@Getter
@NoArgsConstructor
@Data
@Entity
@Table(name = "membershipfees")
public class MembershipFee {

    @Id
    @Column(name = "idFee")
    private Long id;

    @Column(name = "totalAmount")
    private long amount;

    private int paidOn;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    MemberId memberId;

    private boolean paid;

    private long amountPaid;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "club_id", nullable = false))})
    private ClubId clubId;

    public MembershipFee(Long id, long amount, int paidOn, MemberId memberId, boolean paid, long amountPaid, ClubId clubId) {
        this.id = id;
        this.amount = amount;
        this.paidOn = paidOn;
        this.memberId = memberId;
        this.paid = paid;
        this.amountPaid = amountPaid;
        this.clubId = clubId;
    }
    public void setId(long idFee) {
        this.id = idFee;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public void setMemberId(MemberId memberId) {
        this.memberId = memberId;
    }

    public void setClub(ClubId clubId) {
        this.clubId = clubId;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public void setAmountPaid(long amountPaid) {
        this.amountPaid = amountPaid;
    }

}