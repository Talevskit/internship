package com.inellipse.clubmanagement.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.inellipse.clubmanagement.domain.base.DomainObjectId;

import java.util.UUID;

import javax.persistence.Embeddable;
import javax.persistence.PrePersist;

@Embeddable
public class MemberId extends DomainObjectId {

    private MemberId() {
        super(DomainObjectId.randomId(MemberId.class).toString());
    }

    @JsonCreator
    public MemberId(String id) {
        super(id);
    }	
}