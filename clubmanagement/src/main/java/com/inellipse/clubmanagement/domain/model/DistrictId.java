package com.inellipse.clubmanagement.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.inellipse.clubmanagement.domain.base.DomainObjectId;

import javax.persistence.Embeddable;

@Embeddable
public class DistrictId extends DomainObjectId {

    private DistrictId() {
        super(DomainObjectId.randomId(DistrictId.class).toString());
    }

    @JsonCreator
    public DistrictId(String id) {
        super(id);
    }
}

