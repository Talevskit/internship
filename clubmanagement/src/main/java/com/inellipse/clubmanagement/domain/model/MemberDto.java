package com.inellipse.clubmanagement.domain.model;

import com.inellipse.clubmanagement.port.response.MemberResponse;

import lombok.Data;

@Data
public class MemberDto {
	
	private MemberResponse member;
	private MemberId id;

}
