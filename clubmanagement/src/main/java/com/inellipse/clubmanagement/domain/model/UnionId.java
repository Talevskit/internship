package com.inellipse.clubmanagement.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.inellipse.clubmanagement.domain.base.DomainObjectId;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
public class UnionId extends DomainObjectId {

    private UnionId() {
        super(DomainObjectId.randomId(UnionId.class).toString());
    }

    @JsonCreator
    public UnionId(String id) {
        super(id);
    }
}