package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.authentication.Username;
import com.inellipse.clubmanagement.domain.model.Club;
import com.inellipse.clubmanagement.domain.model.ClubId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClubRepository extends JpaRepository<Club, ClubId> {

    List<Club> getClubsByDistrictId(Long districtId);

    Club findClubById(ClubId id);
    
    Boolean existsByName(String name); 
}

