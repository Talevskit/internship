package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.authentication.EmailConfirmation;
import org.springframework.data.repository.CrudRepository;

public interface VerifyEmailRepository extends CrudRepository<EmailConfirmation, String> {
        EmailConfirmation findByToken(String confirmationToken);
}
