package com.inellipse.clubmanagement.domain.model;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.inellipse.clubmanagement.domain.base.DomainObjectId;

import javax.persistence.Embeddable;

@Embeddable
public class ClubId extends DomainObjectId {

    private ClubId() {
        super(DomainObjectId.randomId(ClubId.class).toString());
    }

    @JsonCreator
    public ClubId(String id) {
        super(id);
    }
}
