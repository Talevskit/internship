package com.inellipse.clubmanagement.domain.model;

import lombok.Data;

@Data
public class EmailDto {

	private String subject;
	private String body;
	
}
