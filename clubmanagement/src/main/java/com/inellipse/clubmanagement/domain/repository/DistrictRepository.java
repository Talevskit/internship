package com.inellipse.clubmanagement.domain.repository;


import com.inellipse.clubmanagement.domain.model.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DistrictRepository extends JpaRepository<District, String> {

}
