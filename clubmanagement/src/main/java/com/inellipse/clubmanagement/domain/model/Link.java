package com.inellipse.clubmanagement.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "links")
@NoArgsConstructor
public class Link {

    @Id
    @SequenceGenerator(name="link_sequence",sequenceName = "link_sequence",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "link_sequence")
    private Long id;

    private String url;

    private String name;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "member_id", nullable = false))})
    private MemberId memberId;

    public Link(String url, String name, MemberId memberId) {
        this.url = url;
        this.name = name;
        this.memberId = memberId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMemberId(MemberId memberId) {
        this.memberId = memberId;
    }
}
