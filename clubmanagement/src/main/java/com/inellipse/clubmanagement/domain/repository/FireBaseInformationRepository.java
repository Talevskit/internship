package com.inellipse.clubmanagement.domain.repository;

import com.inellipse.clubmanagement.domain.firebase.FireBaseInformation;
import com.inellipse.clubmanagement.domain.model.FireBaseToken;
import com.inellipse.clubmanagement.domain.model.MemberId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FireBaseInformationRepository extends JpaRepository<FireBaseInformation, Long> {


}
